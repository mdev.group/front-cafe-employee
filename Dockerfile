# Base on offical Node.js Alpine image
FROM node:16-alpine AS installer

# Set working directory
WORKDIR /usr/app

# Copy package.json and package-lock.json before other files
# Utilise Docker cache to save re-installing dependencies if unchanged
COPY ./package*.json ./

# Install dependencies
RUN npm install --production

# Copy all files
COPY ./ ./

# Expose the listening port
EXPOSE 3000

CMD [ "npm", "run dev" ]

# Base on offical Node.js Alpine image
FROM node:16-alpine AS production

# Set working directory
WORKDIR /usr/app

# Copy package.json and package-lock.json before other files
# Utilise Docker cache to save re-installing dependencies if unchanged
COPY ./package*.json ./

# Install dependencies
RUN npm install --production

# Copy all files
COPY ./ ./

RUN npm run build

# Expose the listening port
EXPOSE 3000

CMD [ "npm", "start" ]