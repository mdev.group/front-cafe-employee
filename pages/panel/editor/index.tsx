import classNames from "classnames";
import { seoData as seoDataMock } from "core/constants/seoData";
import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { AddToCart, createOrder, setCurrentOrderID } from "core/redux/actions/cafe";
import { IRootReducer } from "core/redux/reducers";
import { cafeCategories_selector, cafe_selector, selectedOrder_selector, user_selector } from "core/redux/selectors";
import { IItemDTO } from "core/redux/types/dto/item";
import { IOrderItemIngredients } from "core/redux/types/dto/order";
import AlertBanner from "core/uikit/AlertBanner";
import Button from "core/uikit/Button";
import Cart from "core/uikit/Cart";
import Grid from "core/uikit/Grid";
import ItemCard from "core/uikit/page/ItemCard";
import PanelContainer from "core/uikit/PanelContainer";
import redirectTo from "core/utils/redirectTo";
import { uniqueId } from "lodash";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from 'styles/pages/home.module.scss'

const Page: NextPage = () => {
  const dispatch = useDispatch();
  const {query: {orderID}} = useRouter()

  const [categoryFilter, setCategory] = useState(null);

  const userData = useSelector(user_selector);
  const cafeData = useSelector(cafe_selector);
  const cafeCategories = useSelector(cafeCategories_selector);
  const order = useSelector(selectedOrder_selector);
  
  const isLoaded = useDebouncedMemo(()=>{
    return [userData].every(item=>item !== undefined);
  }, [userData], 200)

  const addToOrder = useCallback((item: IItemDTO) => () => {
    if(userData && cafeData) {
      const ingredients: IOrderItemIngredients[] = item.composition.map((assign) => {
        const compData = item.compositionItems.find((comp) => comp._id === assign.id);
        
        if(compData) {
          return {
            id: assign.id,
            defaultCount: assign.isEditable ? 0 : 1,
            count: assign.isEditable ? 0 : 1,
            name: compData.name,
            price: assign.price || 0,
            isEditable: assign.isEditable,
            massStep: item.mass
          }
        }

        return undefined!;
      }).filter((data) => data !== undefined);

      if(!order) {
        dispatch(createOrder({
          cafeID: cafeData._id,
          positions: [{
            itemID: item._id,
            orderTime: Number(new Date()),
            ingredients,
            fixed: false,
            uuid: ''
          }],
          employeeID: userData._id,
        }))
      } else {
        dispatch(AddToCart({
          itemID: item._id,
          orderTime: Number(new Date()),
          ingredients,
          fixed: false,
          uuid: ''
        }))
      }
    }
  }, [dispatch, userData, cafeData, order])

  const handleUpdateCategory = useCallback((value) => () => {
    setCategory(value);
  }, [])

  useEffect(() => {
    dispatch(setCurrentOrderID(orderID?.toString() || null))
  }, [dispatch, orderID])

  return (
    <div>
      <Head>
        <title>{orderID ? 'Редактор заказа | M.Dev Cafe' : 'Создание заказа | M.Dev Cafe'}</title>
      </Head>

      <PanelContainer loading={!isLoaded} className={styles.page__main}>
        {isLoaded && (
          <>
            <div className={styles.page__categories}>
              <button
                className={classNames(styles.page__category, {
                  [styles['page__category--active']]: categoryFilter === null
                })}
                onClick={handleUpdateCategory(null)}
              >
                Все позиции
              </button>
              {cafeCategories?.map((cat, index) => (
                <button
                  key={index}
                  className={classNames(styles.page__category, {
                    [styles['page__category--active']]: categoryFilter === cat
                  })}
                  onClick={handleUpdateCategory(cat)}
                >
                  {cat}
                </button>
              ))}
            </div>
            <Grid
              itemSize="calc((100% - 16px) / 3)"
              gap="8px"
            >
              {cafeData?.items.filter((item) => categoryFilter === null || (item.category === categoryFilter || categoryFilter == 'Без категории' && !item.category)).map((item) => (
                <ItemCard
                  key={item._id}
                  data={item}
                  onClick={addToOrder(item)}
                />
              ))}
            </Grid>

            <Cart Send Clear/>
          </>
        )}
      </PanelContainer>
    </div>
  );
};

export default Page;
