import { setCurrentOrderID } from "core/redux/actions/cafe";
import { orders_selector, selectedOrder_selector } from "core/redux/selectors";
import { EOrderStatus } from "core/redux/types/dto/order";
import Cart from "core/uikit/Cart";
import Grid from "core/uikit/Grid";
import OrderCard from "core/uikit/OrderCard";
import PanelContainer from "core/uikit/PanelContainer";
import redirectTo from "core/utils/redirectTo";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";

const Page: React.FC = () => {
  const {query: {id : orderID}} = useRouter();
  const dispatch = useDispatch();

  const orders = useSelector(orders_selector);

  useEffect(() => {
    if(orders.some((item) => item._id === orderID)) {
      dispatch(setCurrentOrderID(orderID as string))
    }
  }, [orderID, orders, dispatch])

  const ordersFiltered = useMemo(() => {
    return orders.filter((ord) => ord.status !== EOrderStatus.done).sort((ordA, ordB) => ordB.beginTime - ordA.beginTime)
  }, [orders])

  return (
    <div>
      <Head>
        <title>Активные заказы | M.Dev Cafe</title>
      </Head>

      <PanelContainer loading={false} hasCart={false}>
        <h2>Активные заказы</h2>
        <Grid itemSize="calc((100% - 24px) / 4)" gap="8px">
          {ordersFiltered.map((item) => (
            <OrderCard
              key={item._id}
              order={item}
              onClick={() => redirectTo(`/panel/orders/${item._id}`)}
            />
          ))}
        </Grid>
      </PanelContainer>
    </div>
  );
}

export default Page;