import { setCurrentOrderID, updateOrder, checkLoyaltyCode, updateOrderStatus, updateToCart, setLoyaltyUser, doOrderPayment } from "core/redux/actions/cafe";
import { cafe_selector, checkLoyalty_selector, orders_selector, selectedOrderCartTotal_selector, selectedOrder_selector } from "core/redux/selectors";
import { EOrderStatus } from "core/redux/types/dto/order";
import ActionRow from "core/uikit/ActionRow";
import Cart from "core/uikit/Cart";
import PanelContainer from "core/uikit/PanelContainer";
import StatusRow from "core/uikit/StatusRow";
import Head from "next/head";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Check as CheckIcon, Close as CloseIcon, QrCode, CreditCard, Money, MoneyCard, Check } from 'core/uikit/Icon'
import ModalContainer from "core/uikit/ModalContainer";
import SplitNumberInput from "core/uikit/SplitNumberInput";
import styles from 'styles/pages/order.module.scss'
import QrReader from 'react-qr-scanner'
import { Typography } from "core/uikit/Typography";
import Input from "core/uikit/Input";
import SquareButton from "core/uikit/SquareButton";
import { useForm } from "core/hooks/useForm";
import Button from "core/uikit/Button";
import { ELoyaltyType } from "core/redux/types/dto/cafe";

const Page: React.FC = () => {
  const {query: {id : orderID}} = useRouter();
  const dispatch = useDispatch();

  const [showPhoneModal, setShowPhoneModal] = useState(false);
  const [showQR, setShowQR] = useState(false);
  const [showLoyaltyCheck, setShowLoyaltyCheck] = useState(false);

  const [showScoresChoose, setShowScoresChoose] = useState(false);

  const [showSplitModal, setShowSplitModal] = useState(false);

  const cafe = useSelector(cafe_selector);
  const orders = useSelector(orders_selector);
  const order = useSelector(selectedOrder_selector);
  const total = useSelector(selectedOrderCartTotal_selector);
  const checkLoyalty = useSelector(checkLoyalty_selector);

  useEffect(() => {
    if(orders.some((item) => item._id === orderID)) {
      dispatch(setCurrentOrderID(orderID as string))
    }
  }, [orderID, orders, dispatch])

  const handleKitchenDone = useCallback(() => {
    dispatch(updateOrder({
      status: EOrderStatus.serve,
      positions: order?.positions.map((pos) => ({
        ...pos,
        fixed: true
      }))
    }))
  }, [dispatch, order])

  const handleCheckTime = useCallback(() => {
    dispatch(updateOrder({
      status: EOrderStatus.checktime
    }))
  }, [dispatch])

  const handleLoyalty = useCallback((precheck) => () => {
    let nextStatus = EOrderStatus.loyalty;

    if(order?.clientID) {
      nextStatus = EOrderStatus.scoresChoose;
    }
    
    if(cafe?.loyaltyType === ELoyaltyType.DISABLED) {
      nextStatus = EOrderStatus.paySelection;
    }

    dispatch(updateOrder({
      status: nextStatus,
      precheck
    }))
  }, [dispatch, order, cafe])


  const handleQRLoyalty = useCallback(() => {
    setShowQR(true)
  }, [])

  const handlePhoneLoyalty = useCallback(() => {
    setShowPhoneModal(true)
  }, [])

  const handleLoyaltyCodeSelected = useCallback((code) => {
    dispatch(checkLoyaltyCode(code));
  }, [dispatch])

  const handleLoyaltyAccept = useCallback(() => {
    dispatch(updateOrder({
      status: EOrderStatus.scoresChoose,
      clientID: checkLoyalty?.res?._id
    }));
    setShowLoyaltyCheck(false);
    setShowPhoneModal(false);
    setShowQR(false);
  }, [dispatch, checkLoyalty])


  const onScan = useCallback((data) => {
    if(data) {
      try{
        const qrdata = JSON.parse(data.text);
        if(qrdata.code && qrdata.code.length === 6) {
          setShowQR(false);
          handleLoyaltyCodeSelected(qrdata.code)
          setShowLoyaltyCheck(true);
        }
      } catch {

      }
    }
  }, [handleLoyaltyCodeSelected])

  const handlePayTime = useCallback(() => {
    dispatch(updateOrder({
      status: EOrderStatus.paySelection
    }))
  }, [dispatch])


  const handleAccept = useCallback(() => {
    dispatch(updateOrder({
      status: EOrderStatus.approved
    }))
  }, [dispatch])

  var {
    onChangeField,
    formData,
  } = useForm(() => {}, {});

  useEffect(() => {
    dispatch(setLoyaltyUser(order?.clientID || null));
  }, [dispatch, order])

  const [scores, setScoresToUse] = useState(0);
  const handleUpdateScoresUsage = useCallback((value) => {
    setScoresToUse(Math.max(0, Math.min(value, checkLoyalty?.res?.scores || 0)))
  }, [checkLoyalty])

  const handleSetScoresUsage = useCallback(() => {
    dispatch(updateOrder({
      status: EOrderStatus.paySelection,
      payByScores: Number(scores)
    }))

    setShowScoresChoose(false);
  }, [dispatch, scores])

  const handleDone = useCallback((payCard) => {
    dispatch(updateOrder({
      status: EOrderStatus.paytime,
      payByCard: payCard,
      payByPaper: (total.cost - (order?.payByScores || 0)) - payCard
    }))
    setShowSplitModal(false);
  }, [dispatch, total, order])

  const handleCloseOrder = useCallback(() => {
    dispatch(doOrderPayment())
  }, [dispatch])

  return (
    <>
      {showQR && (
        <div className={styles.page__qrWrapper} onClick={() => {setShowQR(false)}}>
          <QrReader
            delay={1000}
            onScan={onScan}
            onError={() => {}}
          />
        </div>
      )}

      {showPhoneModal && (
        <ModalContainer onClose={() => setShowPhoneModal(false)}>
          <div className={styles.page__phoneModalForm}>
            <h3>Код пользователя</h3>
            <SplitNumberInput
              sets={[3, 3]}
              onSend={(value) => {handleLoyaltyCodeSelected(value); setShowLoyaltyCheck(true);}}
              success={false}
            />
          </div>
        </ModalContainer>
      )}

      {showLoyaltyCheck && (
        <ModalContainer onClose={() => setShowLoyaltyCheck(false)}>
          <div className={styles.page__phoneModalForm}>
            <h3>Проверка</h3>
            {checkLoyalty?.res !== undefined && (
              <div className={styles.page__phoneModalFormRes}>
                {checkLoyalty.res == null && <Typography styleAs='callout' color="#f00">Ошибочный код пользователя</Typography>}
                {checkLoyalty.res !== null && (
                  <div>
                    <Typography  styleAs='callout' color="#2E78E8">
                      {[checkLoyalty.res.first_name, checkLoyalty.res.last_name].join(' ')}
                    </Typography>
                    <Button onClick={handleLoyaltyAccept}>Ок</Button>
                  </div>
                )}
              </div>
            )} 
          </div>
        </ModalContainer>
      )}

      {showScoresChoose && (
        <ModalContainer onClose={() => setShowScoresChoose(false)}>
          <div className={styles.page__scoresModalForm}>
            <Typography styleAs='body'>Списание баллов</Typography>
            <div>
              <div>
                <Typography styleAs='footnote'>Текущий балалнс: {checkLoyalty?.res?.scores} руб.</Typography>
                <Input
                  placeholder="Баллы"
                  value={scores}
                  onChange={handleUpdateScoresUsage}
                />
              </div>
              <SquareButton
                theme='success'
                label="Сохранить"
                Icon={CheckIcon}
                borders
                onClick={handleSetScoresUsage}
              />
            </div>
          </div>
        </ModalContainer>
      )}

      {showSplitModal && (
        <ModalContainer onClose={() => setShowSplitModal(false)}>
          <div className={styles.page__moneyModalForm}>
            <Typography styleAs='body'>Разделить оплату</Typography>
            <Typography styleAs='footnote'>Необходимая сумма: {total.cost - (order?.payByScores || 0)} руб.</Typography>
            <Typography styleAs='caption-2'>Укажите сумму оплаты картой.</Typography>
            <div>
              <div>
                <div>
                  <CreditCard size='lg'/>
                  <Input
                    placeholder="Картой"
                    value={formData.card}
                    onChange={(value) => {
                      onChangeField('card')(Math.max(0, Math.min(value, total.cost - (order?.payByScores || 0))) as any)
                    }}
                  />
                </div>
                <div>
                  <Money size='lg'/>
                  <Input
                    placeholder="Наличными"
                    value={Number(total.cost - (order?.payByScores || 0) - formData.card) || 0}
                    disabled
                  />
                </div>
              </div>
              <SquareButton
                theme='success'
                label="Сохранить"
                Icon={CheckIcon}
                borders
                onClick={() => [handleDone(formData.card)]}
              />
            </div>
          </div>
        </ModalContainer>
      )}



      <div>
        <Head>
          <title>Заказ №{order?.number}</title>
        </Head>

        <PanelContainer loading={false}>
          <Cart
            Send={order?.status === EOrderStatus.form}
            Edit
            Clear
          />

          <StatusRow.Row label="Формируется" active={(order?.status || 0) >= EOrderStatus.form || null}>
            <StatusRow.Row label="Создан" active={(order?.status || 0) >= EOrderStatus.created || null}>
              <StatusRow.Row label="Принят в работу" active={(order?.status || 0) >= EOrderStatus.approved || null}>
                
                { order?.positions.some((pos) => !pos.fixed) && (
                  <ActionRow.Row label="Передан на кухню?">
                    <ActionRow.Action
                      label="Готово"
                      theme='success'
                      Icon={CheckIcon}
                      handle={handleKitchenDone}
                    />
                  </ActionRow.Row>
                )}

                <StatusRow.Row label="На кухне" active={(order?.status || 0) >= EOrderStatus.serve && !order?.positions.some((pos) => !pos.fixed)}>
                  {order?.status === EOrderStatus.serve && (
                    <ActionRow.Row label="Дальнейшие действия">
                      <ActionRow.Action
                        label="К расчету"
                        theme='success'
                        Icon={CheckIcon}
                        handle={handleCheckTime}
                      />
                    </ActionRow.Row>
                  )}
                </StatusRow.Row>

              </StatusRow.Row>
            </StatusRow.Row>
          </StatusRow.Row>

          {(order?.status || 0) >= EOrderStatus.checktime && (
            <StatusRow.Row label="Расчет начат" active/>
          )}

          {(order?.status || 0) >= EOrderStatus.loyalty && (
            <StatusRow.Row label="Пречек" active={order?.precheck !== undefined ? order?.precheck : null}/>
          )}
          
          {(order?.status || 0) >= EOrderStatus.scoresChoose && (
            <StatusRow.Row
              label={checkLoyalty?.res ? (
                `Программа лояльности (${[checkLoyalty?.res?.first_name, checkLoyalty?.res?.last_name].join(' ')})`
              ) : (
                `Программа лояльности не используется`
              )}
              active={(!!order?.clientID) || false}
            />
          )}

          {(order?.status || 0) >= EOrderStatus.paySelection && (
            <>
              {order?.clientID && (
                <StatusRow.Row label={order?.payByScores ? `Будет списано: ${order?.payByScores} баллов` : 'Баллы лояльности не будут списаны'} active={order?.payByScores !== undefined ? !!order.payByScores : null}/>
              )}
            </>
            
          )}

          {(order?.status || 0) >= EOrderStatus.paytime && (
            <>
              <StatusRow.Row label={order?.payByCard ? `Оплата картой: ${order?.payByCard} руб.` : 'Без оплаты картой'} active={true}/>
              <StatusRow.Row label={order?.payByPaper ? `Оплата наличными: ${order?.payByPaper} руб.` : 'Без оплаты наличными'} active={true}/>
            </>
          )}

          {(order?.status || 0) >= EOrderStatus.done && (
            <StatusRow.Row label="Заказ выполнен" active={true}/>
          )}

          {order?.status == EOrderStatus.created && (
            <ActionRow.Row label="Пречек">
              <ActionRow.Action
                label="Принят"
                theme='success'
                Icon={CheckIcon}
                handle={handleAccept}
              />
            </ActionRow.Row>
          )}


          {order?.status == EOrderStatus.checktime && (
            <ActionRow.Row label="Пречек">
              <ActionRow.Action
                label="У клиента"
                theme='success'
                Icon={CheckIcon}
                handle={handleLoyalty(true)}
              />
              <ActionRow.Action
                label="Без пречека"
                theme='danger'
                Icon={CloseIcon}
                handle={handleLoyalty(false)}
              />
            </ActionRow.Row>
          )}


          {(order?.status || 0) == EOrderStatus.loyalty && (
            <>
              <ActionRow.Row label="Программа лояльности">
                <ActionRow.Action
                  label="По QR"
                  Icon={QrCode}
                  handle={handleQRLoyalty}
                />
                <ActionRow.Action
                  label="По коду"
                  Icon={CheckIcon}
                  handle={handlePhoneLoyalty}
                />
                <ActionRow.Action
                  label="Отказ"
                  theme='danger'
                  Icon={CloseIcon}
                  handle={handlePayTime}
                />
              </ActionRow.Row>
            </>
          )}


          {(order?.status || 0) == EOrderStatus.scoresChoose && (
            <ActionRow.Row label={checkLoyalty?.res?.scores ? `Списание бонусов. ${checkLoyalty?.res?.scores} б. на счету` : 'Нет бонусов на счету'}>
              {checkLoyalty?.res?.scores && (
                <ActionRow.Action
                  label="Списать"
                  Icon={CheckIcon}
                  handle={() => {setShowScoresChoose(true)}}
                />
              )}
              <ActionRow.Action
                label="Не списывать"
                theme='danger'
                Icon={CloseIcon}
                handle={handleSetScoresUsage}
              />
            </ActionRow.Row>
          )}

          {(order?.status || 0) == EOrderStatus.paySelection && (
            <ActionRow.Row 
            label={order?.clientID ? (
              `Выбор способа оплаты (${total.cost - (order?.payByScores || 0)} руб. + ${order?.payByScores} б.)`
            ) : (
              `Выбор способа оплаты (${total.cost} руб.)`
            )}>
              <ActionRow.Action
                label="Наличными"
                Icon={Money}
                handle={() => handleDone(0)}
              />
              <ActionRow.Action
                label="Частично"
                Icon={MoneyCard}
                handle={() => setShowSplitModal(true)}
              />
              <ActionRow.Action
                label="Картой"
                Icon={CreditCard}
                handle={() => handleDone(total.cost - (order?.payByScores || 0))}
              />
            </ActionRow.Row>
          )}


          {(order?.status || 0) == EOrderStatus.paytime && (
            <ActionRow.Row label={`Закрытие заказа`}>
              <ActionRow.Action
                label="Готово"
                Icon={CheckIcon}
                handle={handleCloseOrder}
              />
            </ActionRow.Row>
          )}

        </PanelContainer>
      </div>
    </>
  );
}

export default Page;