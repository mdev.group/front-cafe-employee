/* eslint-disable @next/next/no-sync-scripts */
import "../styles/globals.css";
import type { AppProps } from "next/app";
import Head from "next/head";
import { initStore, getStore } from "../core/redux/index";
import { Provider } from "react-redux";
import { useEffect, useRef, useState } from "react";
import authTokenService from "core/services/authTokenService";
import { TStore } from "core/redux/storeDataProvider";
import Navbar from "core/uikit/Navbar";
import { useRouter } from "next/router";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import Button from "core/uikit/Button";
import ModalFooter from "core/uikit/ModalContainer/components/ModalFooter/ModalFooter";
import ModalHeader from "core/uikit/ModalContainer/components/ModalHeader/ModalHeader";

function MyApp({ Component, pageProps }: AppProps) {
  const [store, setStore] = useState<TStore>();
  const [needInteract, setNeedInteract] = useState(true);

  useEffect(() => {
    initStore();
    const store = getStore();
    authTokenService.init(store.dispatch);
    setStore(store);
  }, [])

  const router = useRouter();

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=no, viewport-fit=cover"
        />
        <link rel="manifest" href="manifest.json"/>
        <script type="module" src="/pwabuilder-sw-register.js"/>
      </Head>
      {store && (
        <Provider store={store}>
          {router.asPath.indexOf('/panel') !== -1 && <Navbar />}
          {/* {needInteract && (
            <ModalContainer onClose={() => setNeedInteract(false)}>
              <div>
                <ModalHeader>
                 Добро пожаловать
                </ModalHeader>
                <ModalFooter>
                  <Button variant='filled' theme='primary' onClick={() => setNeedInteract(false)}>Начать использование</Button>
                </ModalFooter>
              </div>
            </ModalContainer>
          )} */}
          <Component {...pageProps} />
        </Provider>
      )}
    </>
  );
}

export default MyApp;
