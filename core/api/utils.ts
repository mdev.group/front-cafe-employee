import superagent from 'superagent';
import { Response } from 'superagent';
import queryString from 'query-string';
import request from './request';
import authTokenService from '../services/authTokenService';
import { IQueryStringObject } from 'core/types/types';

export type TRefreshToken = () => Promise<any>;

export const refreshToken: TRefreshToken = async () => {
    const refreshData = authTokenService.getRefreshData();
    if(refreshData){
        return await request.post('/auth/refresh', {
            token: refreshData?.refreshToken
        }).catch(()=>{
            authTokenService.clearToken();
            authTokenService.clearRefreshData();
        }).then((res: any)=>{
            console.log('[Auth] New token pair recieved')

            authTokenService.setRefreshData({
                refreshToken: res.refresh_token,
                accessTokenLifeTime: res.expires_in
            });
            authTokenService.setToken(res.access_token)
        });
    }
    return false;
};


type TGetResponseBody = (response: void | Response) => any;

export const getResponseBody: TGetResponseBody = (response) => response && response.body;

export const dropEmptyOrWrongParams = (qso: IQueryStringObject): IQueryStringObject => {
    return Object.keys(qso).reduce((result, paramKey) => {
        return (qso[paramKey]?.toString())
            ? {
                ...result,
                [paramKey]: qso[paramKey]
            }
            : result;
    }, {});
};

export const addTailControlSequences = (url: string): string => {
    return !url.includes('?') ? `${url}?` : `${url}&`;
};

export const addNotEmptyParamsToUrl = (url: string, qso: IQueryStringObject): string => {
    const params = queryString.stringify(dropEmptyOrWrongParams(qso));
    return (params) ? `${addTailControlSequences(url)}${params}` : url;
};

export const addParamsToUrl = (url: string, qso: IQueryStringObject): string => {
    const params = queryString.stringify(qso);
    return (params) ? `${addTailControlSequences(url)}${params}` : url;
};
