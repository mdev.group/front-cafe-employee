export {
  default
} from './api';

export const host = process.env.NEXT_PUBLIC_BACK_BASE_URL;