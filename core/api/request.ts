import superagent from 'superagent';

import { getResponseBody } from './utils';

import { ICustomErrorObject, TFunc } from '../types/types';
import authTokenService from '../services/authTokenService';

export type TCustomError = TFunc | ICustomErrorObject;

export const host = process.env.NEXT_PUBLIC_BACK_BASE_URL;

interface IRequestType {
  [type: string]: any;
}

const request:IRequestType = {
    get: <T>(
        path: string,
        requestData?: Record<string, any>,
        options?: {
            isBinary?: boolean;
            onError?: TCustomError;
        }
    ) => superagent.get(`${host}${path}`)
        .responseType(options?.isBinary ? 'blob' : '')
        .query(requestData as any)
        .use(authTokenService.tokenPlugin)
        .use(authTokenService.refreshToken)
        // .catch((e) => handleError(e, options?.onError))
        .then<T>(getResponseBody),
    post: <T>(
        path: string,
        requestData?: Record<string, any>,
        options?: {
            onError?: TCustomError;
        }
    ) => superagent.post(`${host}${path}`)
        .send(requestData)
        .use(authTokenService.tokenPlugin)
        .use(authTokenService.refreshToken)
        // .catch((e) => handleError(e, options?.onError))
        .then<T>(getResponseBody),
    patch: <T>(
        path: string,
        requestData?: Record<string, any>,
        options?: {
            onError?: TCustomError;
        }
    ) => superagent.patch(`${host}${path}`)
        .send(requestData)
        .use(authTokenService.tokenPlugin)
        .use(authTokenService.refreshToken)
        // .catch((e) => handleError(e, options?.onError))
        .then<T>(getResponseBody),
    delete: <T>(
        path: string,
        requestData?: Record<string, any>,
        options?: {
            onError?: TCustomError;
        }
    ) => superagent.delete(`${host}${path}`)
        .send(requestData)
        .use(authTokenService.tokenPlugin)
        .use(authTokenService.refreshToken)
        // .catch((e) => handleError(e, options?.onError))
        .then<T>(getResponseBody)
};

export default request;
