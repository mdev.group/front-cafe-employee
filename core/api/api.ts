import { ICafeDTO, IUpdateCafeDTO } from 'core/redux/types/dto/cafe';
import { IClient } from 'core/redux/types/dto/client';
import { IEmployeeDTO } from 'core/redux/types/dto/employee';
import { IComposition, IItemDTO } from 'core/redux/types/dto/item';
import { IOrder } from 'core/redux/types/dto/order';
import { IUserDTO } from 'core/redux/types/dto/user';
import { IWorkShiftDTO } from 'core/redux/types/dto/workshift';
import { removeEmptyFields } from 'core/utils/removeEmptyFields';
import getApi from './getApi';
import { employeeFields, generateCafeGetter, generateGetter } from './structures';
import { refreshToken } from './utils';

export enum ERequestTypes {
    GET = 'get',
    POST = 'post',
    PATCH = 'patch',
    DELETE = 'delete'
}

export interface IApi {
    refreshToken: () => any;

    authUser: (data: any) => any;

    getUserData: () => IUserDTO;
    getCafeData: () => ICafeDTO;

    readAllComposition: () => IComposition[];

    createOrder: (data: Partial<IOrder>) => IOrder;
    updateOrder: ({id, data}: {id: string, data: Partial<IOrder>}) => IOrder;
    doOrderPayment: ({orderID}: {orderID: string}) => IOrder;
    deleteOrder: ({id}: {id: string}) => boolean;
    getActiveOrders: ({cafeID}: {cafeID: string}) => IOrder;

    getClientByCode: ({code}: {code: string}) => IClient;
    getClientByID: ({id}: {id: string}) => IClient;
}

const api: IApi = getApi({
    refreshToken: {
        requestHandler: refreshToken
    },
    authUser: {
        path: () => '/auth/login/employee',
        type: ERequestTypes.POST
    },

    registerUser: {
        path: () => '/auth/register',
        type: ERequestTypes.POST
    },

    getUserData: {
        path: () => '/user/self',
        type: ERequestTypes.GET
    },

    getCafeData: {
        path: () => '/cafe/my',
        type: ERequestTypes.GET
    },

    readAllComposition: {
        path: () => `/composition`,
        type: ERequestTypes.GET
    },

    createOrder: {
        path: () => `/order`,
        type: ERequestTypes.POST
    },

    updateOrder: {
        path: ({id}) => `/order/${id}`,
        type: ERequestTypes.PATCH,
        dataHandler: ({data}) => data
    },

    deleteOrder: {
        path: ({id}) => `/order/${id}`,
        type: ERequestTypes.DELETE,
        dataHandler: () => ({})
    },

    getActiveOrders: {
        path: ({cafeID}) => `/order/cafe/${cafeID}`,
        type: ERequestTypes.GET,
        dataHandler: () => ({})
    },

    getClientByCode: {
        path: ({code}) => `/client/code/${code}`,
        type: ERequestTypes.GET,
        dataHandler: () => ({})
    },

    getClientByID: {
        path: ({id}) => `/client/${id}`,
        type: ERequestTypes.GET,
        dataHandler: () => ({})
    },

    doOrderPayment: {
        path: () => `/order/payment`,
        type: ERequestTypes.POST,
        dataHandler: ({orderID}) => ({orderID})
    },
});

export default api;