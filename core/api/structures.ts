export const employeeFields: TFileds = [
  'ID',
  'birthDate',
  'code',
  'dismissalDate',
  'firstname',
  'hiringDate',
  'image',
  'patronymic',
  'phone',
  'position',
  'rating',
  'role',
  'surname'
]

export const cafeFields: TFileds = [
  'ID',
  'creationDate',
  'image',
  'loyaltyType',
  'name',
  'closed',
  'address',
  ['employees', employeeFields]
]

type TFileds = (string | [key: string, fields: TFileds])[];

const convertFields = (fields: TFileds): string => {
  return (fields.reduce((prev, curr) => {
    if(typeof curr === 'string') {
      return prev + `, ${curr}`
    }
    return prev + `, ${curr[0]} { ${convertFields(curr[1])} }`
  }, '') as string).substring(2);
}


export const generateCafeGetter = ():string => {
  return convertFields(cafeFields);
}

export const generateGetter = (fields: TFileds):string => {
  return convertFields(fields);
}