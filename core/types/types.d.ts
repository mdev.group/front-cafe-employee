import { ResponseError } from "superagent";

export interface ICustomErrorObject {
  check: (e: ResponseError, response: Record<string, any>) => boolean;
  process: (e: ResponseError, response: Record<string, any>) => void;
}

export interface IQueryStringObject {
  [key: string]: string | number;
}

export type TFunc<T = void> = (...args : any) => T;