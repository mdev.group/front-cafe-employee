import classNames from 'classnames';
import styles from './Badge.module.scss';

interface IProps {
    className?: string; 
}

const Badge: React.FC<IProps> = ({
    children,
    className
}) => {
    return (
        <div className={classNames(className, styles.badge)}>
            {children}
        </div>
    )
}

export default Badge;