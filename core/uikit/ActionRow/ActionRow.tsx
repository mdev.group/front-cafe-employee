import {
  ChangeEvent,
  ChangeEventHandler,
  FunctionComponent,
  useState,
} from "react";
import styles from "./ActionRow.module.scss";
import cn from "classnames";
import { Typography } from "../Typography";
import { Check, Close } from "../Icon/Icons";
import SquareButton from "../SquareButton";
import { CreateIconProps } from "../Icon/Icon";

type RowType = {
  label: string;
};
type ActionType = {
  label: string;
  handle?: () => void;
  theme?: "default" | "danger" | "success";
  Icon?: FunctionComponent<CreateIconProps>;
};

export const Action: FunctionComponent<ActionType> = ({ label, handle, theme, Icon }) => {
  const handleClick = () => {
    if (handle) handle();
  };

  return <SquareButton label={label} onClick={handleClick} borders theme={theme} Icon={Icon}/>;
};

export const Row: FunctionComponent<RowType> = ({ label, children }) => {
  return (
    <div className={cn(styles.Row)}>
      <Typography styleAs="body" color="#111" className={styles.Row__label}>
        {label}
      </Typography>

      <div className={styles.Actions}>
        {children}
      </div>
    </div>
  );
};
