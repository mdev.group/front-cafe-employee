import classNames from 'classnames';
import styles from './Typo.module.scss';

type TVariants = 'title-24' | 'title-20' | 'title-16-b' | 'title-16' | 'title-14' | 'title-12' | 'rubrik'

interface IProps {
  variant: TVariants;
  className?: string;
}

const Typo: React.FC<IProps> = ({
  variant,
  children,
  className
}) => {
  return (
    <span className={classNames(className, styles.typo, styles[`typo--${variant}`])}>
      {children}
    </span>
  )
}

export default Typo;