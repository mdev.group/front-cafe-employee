import classNames from 'classnames';
import { TFunc } from 'core/types/types';
import { useMemo, useState } from 'react';
import styles from './Toggle.module.scss';

interface IProps {
  invert?: boolean;
  value?: boolean;
  onChange?: TFunc;
}

const Toggle: React.FC<IProps> = ({
  invert,
  value: controlledValue,
  onChange
}) => {
  const [value, setValue] = useState<any>(false);

  const fieldValue = useMemo(()=>{
    return controlledValue || value;
  }, [value, controlledValue])

  const handleChange = ()=>{
    setValue(!fieldValue);
    if(onChange) onChange(!fieldValue)
  }
  
  return (
    <button onClick={handleChange} className={classNames(styles.toggle, {
      [styles.toggle__active]: (invert && !fieldValue) || (!invert && fieldValue)
    })}>
      <div className={styles.toggle__inner}></div>
    </button>
  )
}

export default Toggle;