import classNames from "classnames";
import { TFunc } from "core/types/types";
import Button from "../Button";
import styles from "./AlertBanner.module.scss"

interface IProps {
  title: string;
  buttonText?: string;
  onClick?: TFunc;
}

const AlertBanner: React.FC<IProps> = ({
  buttonText,
  title,
  onClick
})=>{
  return (
    <div className={styles.alertBanner}>
      <span className={styles.alertBanner__title}>{title}</span>
      {buttonText && <Button
        className={styles.alertBanner__button}
        theme='primary'
        size='md'
        variant='outline'
        round
        onClick={onClick}
      >
        {buttonText}
      </Button>}
    </div>
  )
}

export default AlertBanner;