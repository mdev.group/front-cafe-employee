import { FunctionComponent, MouseEventHandler, ReactChild, ReactElement } from "react";
import styles from './Typography.module.scss';
import cn from 'classnames';

type asTag = 'span' | 'p' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
type styleAs = 'largeTitle' | 'title-1' | 'title-2' | 'title-3' | 'headLine' | 'body' | 'callout' | 'subhead' | 'footnote' | 'caption-1' | 'caption-2';
type TypeographyProps = {
  className?: string;
  as?: asTag;
  color?: string;
  styleAs?: styleAs;
};

export const Typography: FunctionComponent<TypeographyProps> = ({
  className,
  as = 'span',
  color,
  children,
  styleAs
}) => {

  const classes = cn(styles.Typography, styles[`as-${as}`], styles[`styleAs-${styleAs}`], className);
  const CustomTag = as as keyof JSX.IntrinsicElements;

  return (
    <CustomTag className={classes} style={{...(color ? {color: color} : {})}}>
      {children}
    </CustomTag>
  );
};
