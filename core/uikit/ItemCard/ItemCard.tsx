import {
  FunctionComponent,
  MouseEventHandler,
  useState,
  ReactNode,
  PointerEventHandler,
} from "react";
import styles from "./ItemCard.module.scss";
import cn from "classnames";
import { Typography } from "../Typography";
import { ExtendItemCard } from "./ExtendItemCard";
import { usePortal } from "../../hooks/usePortal";
import * as ReactDOM from "react-dom";
import { dispatchAction } from "../../../redux";
import { getOrderByID } from "../../../redux/reducers/items";
import { useSelector } from "react-redux";
import { Item } from "../../../types/types";
import { useWebsoket } from "../../hooks/useWebsoket";

type ItemCardProps = {
  className?: string;
  item: Item;
};

const Modal = ({ children }: { children: ReactNode }) => {
  const target = usePortal("overlay-portal");
  return ReactDOM.createPortal(children, target);
};

export const ItemCard: FunctionComponent<ItemCardProps> = ({
  className,
  item,
}) => {
  const classes = cn(className, styles.ItemCard, styles.pointer);
  const [extend, setExtend] = useState(false);
  const activeOrder = useSelector(getOrderByID());
  const socket = useWebsoket();

  const handleAdd = () => {
    socket.call({
      type: "addToCart",
      payload: {
        orderID: activeOrder?.ID,
        item,
      },
    }).then((result: any) => {
      if (result.code == 200) {
        dispatchAction({
          type: "SET_EDIT_OREDR_ID",
          payload: result.data.order.ID,
        });
      }
    });
  };

  const handleMouseDown: MouseEventHandler<HTMLElement> = (event) => {
    event.stopPropagation();
    event.preventDefault();

    console.log(event)

    const targetElem = event.currentTarget;

    const shortClickCb = () => {
      window.clearTimeout(longClickTimer);
      handleClick();
    };

    const cancelClickCb = () => {
      window.clearTimeout(longClickTimer);
      targetElem.removeEventListener("pointerup", shortClickCb);
    };

    targetElem.addEventListener("mouseup", shortClickCb, {
      once: true,
    });

    targetElem.addEventListener("mouseout", cancelClickCb, {
      once: true,
    });

    targetElem.addEventListener("mouseleave", cancelClickCb, {
      once: true,
    });

    var longClickTimer = window.setTimeout(() => {
      targetElem.removeEventListener("mouseup", shortClickCb);
      handleLongClick();
    }, 500);
  };

  const handlePointerDown: PointerEventHandler<HTMLElement> = (event) => {
    event.stopPropagation();
    event.preventDefault();

    const targetElem = event.currentTarget;

    const shortClickCb = () => {
      window.clearTimeout(longClickTimer);
      handleClick();
    };

    const cancelClickCb = () => {
      window.clearTimeout(longClickTimer);
      targetElem.removeEventListener("pointerup", shortClickCb);
    };

    targetElem.addEventListener("pointerup", shortClickCb, {
      once: true,
    });

    targetElem.addEventListener("pointerout", cancelClickCb, {
      once: true,
    });

    targetElem.addEventListener("pointerleave", cancelClickCb, {
      once: true,
    });

    var longClickTimer = window.setTimeout(() => {
      targetElem.removeEventListener("pointerup", shortClickCb);
      handleLongClick();
    }, 500);
  };

  const handleClick = () => {
    if (handleAdd) handleAdd();
  };

  const handleLongClick = () => {
    setExtend(true);
  };

  return (
    <>
      {extend && (
        <Modal>
          <ExtendItemCard
            onClose={() => {
              setExtend(false);
            }}
            item={item}
          />
        </Modal>
      )}
      <div
        className={classes}
        onMouseDown={handleMouseDown}
        onPointerDown={handlePointerDown}
      >
        <picture className={styles.Picture}>
          <img src={item.image} alt="" />

          <div className={styles.Price}>
            <Typography styleAs="callout" as="span">{`${item.price} ₽`}</Typography>
          </div>
        </picture>
        <div className={styles.InfoBlock}>
          <Typography styleAs="title-3" as="span">
            {item.name}
          </Typography>
        </div>
      </div>
    </>
  );
};
