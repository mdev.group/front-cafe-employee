import {
  FunctionComponent,
  MouseEventHandler,
  ReactNode,
  useEffect,
  useMemo,
  useState,
} from "react";
import styles from "./ExtendItemCard.module.scss";
import cn from "classnames";
import { Typography } from "../Typography";
import Button from "core/uikit/Button";
import CountButton from "../CountButton";

import * as ReactDOM from "react-dom";
import { usePortal } from "../../hooks/usePortal";
import { cafeItemByID_selector, selectedOrder_selector } from "core/redux/selectors";
import { useDispatch, useSelector } from "react-redux";
import { IOrderPosition } from "core/redux/types/dto/order";
import Image from "../Image";
import { AddToCart, removeFromCart, updateToCart } from "core/redux/actions/cafe";

type ExtendItemCardProps = {
  className?: string;

  item: IOrderPosition;

  onSave?: (UID: string, item?: any) => void;
  onClose?: () => void;
};

export const ExtendItemCard: FunctionComponent<ExtendItemCardProps> = ({
  className,
  item,
  onClose,
  onSave,
}) => {
  const dispatch = useDispatch();
  const activeOrder = useSelector(selectedOrder_selector);
  const itemData = useSelector(cafeItemByID_selector(item.itemID));

  const [currIngredients, setIngredients] = useState(
    item.ingredients.map((ing) => ({
      ...ing,
      count: ing.count !== undefined ? ing.count : ing.defaultCount,
    }))
  );

  const [canClose, setCanClose] = useState(false);
  const classes = cn(className, styles.ExtendItemCard);

  const [secModal, setSecModal] = useState(false);
  const SecModal = ({ children }: { children: ReactNode }) => {
    const target = usePortal("overlay-portal-2");
    return ReactDOM.createPortal(children, target);
  };


  const onBack = (event: PopStateEvent)=>{
    history.pushState(null, "", "");
    if (onClose) onClose();
  }

  useEffect(() => {
    window.setTimeout(() => {
      setCanClose(true);
    }, 0);

    history.pushState(null, "", "#modal");

    window.addEventListener('popstate', onBack, false);

    return ()=>{
      window.removeEventListener('popstate', onBack);
    }
  }, []);

  const handleClose: MouseEventHandler<HTMLElement> = (event) => {
    event.stopPropagation();
    event.preventDefault();

    if (event.currentTarget != event.target) {
      return;
    }

    if (onClose && canClose) onClose();
  };

  const handleAdd = () => {
    // socket
    //   .call({
    //     type: "addToCart",
    //     payload: {
    //       orderID: activeOrder?.ID,
    //       item: {
    //         ...item,
    //         ingredients: currIngredients,
    //       },
    //     },
    //   })
    //   .then((result: any) => {
    //     if (result.code == 200) {
    //       dispatchAction({
    //         type: "SET_EDIT_OREDR_ID",
    //         payload: result.data.order.ID,
    //       });
    //     }
    //   });

    if (onClose) onClose();
  };

  const handleRemove = (moderCode?: string) => {
    if (item.uuid) {
      if (item.fixed && !moderCode) {
        setSecModal(true);
      } else {
        dispatch(removeFromCart(item.uuid))
      }
    }
  };

  const handleSave = () => {
    if (item.uuid) {
      dispatch(updateToCart({
        id: item.uuid,
        newData: {
          ingredients: currIngredients
        }
      }))
    }

    if (onClose) onClose();
  };


  const totalPFC = useMemo(() => {
    return currIngredients.reduce((prev, ing) => {
      const compData = itemData?.compositionItems.find((comp) => comp._id === ing.id);
      if(compData && compData.pfc) {
        const {proteins, fats, carbohydrates} = compData.pfc;
        prev.proteins += +proteins * (ing.count * ing.massStep! / 100);
        prev.fats += +fats * (ing.count * ing.massStep! / 100);
        prev.carbohydrates += +carbohydrates * (ing.count * ing.massStep! / 100);
      }
      return prev;
    }, {
      proteins: 0, fats: 0, carbohydrates: 0
    })
  }, [item, currIngredients]);

  return (
    <>
      <div className={classes}>
        <div className={styles.main}>
          <div className={styles.info}>
            <picture className={styles.Picture}>
              <Image src={itemData?.photo_url || ''} alt="" />

              <div className={styles.Price}>
                <Typography
                  styleAs="callout"
                  as="span"
                >{`${itemData?.price} ₽`}</Typography>
              </div>
            </picture>

            <Typography styleAs="title-3" as="span" color="#000">
              {itemData?.name}
            </Typography>

            {totalPFC && (
              <div className={styles.ExtendItemCard__pfcWrapper}>
                <div className={styles.ExtendItemCard__pfc}>
                  <span className={styles.ExtendItemCard__pfcTitle}>Белки</span>
                  <span>{totalPFC.proteins.toFixed(1)}</span>
                </div>
                <div className={styles.ExtendItemCard__pfc}>
                  <span className={styles.ExtendItemCard__pfcTitle}>Жиры</span>
                  <span>{totalPFC.fats.toFixed(1)}</span>
                </div>
                <div className={styles.ExtendItemCard__pfc}>
                  <span className={styles.ExtendItemCard__pfcTitle}>Углеводы</span>
                  <span>{totalPFC.carbohydrates.toFixed(1)}</span>
                </div>
              </div>
            )}
          </div>
          <div className={styles.ingredients}>
            {currIngredients.map((ing) => {
              return (
                <CountButton
                  value={ing.count}
                  title={ing.name}
                  subtitle={`${ing.price} руб.`}
                  key={ing.id}
                  onAdd={() => {
                    setIngredients([
                      ...currIngredients.map((updIng) =>
                        updIng.id == ing.id
                          ? {
                              ...updIng,
                              count: updIng.count + 1,
                            }
                          : updIng
                      ),
                    ]);
                  }}
                  onSub={() => {
                    setIngredients([
                      ...currIngredients.map((updIng) =>
                        updIng.id == ing.id
                          ? {
                              ...updIng,
                              count: updIng.count - 1,
                            }
                          : updIng
                      ),
                    ]);
                  }}
                />
              );
            })}
          </div>
        </div>
        <div className={styles.actions}>
          <Button disabled theme="secondary" size="md">
            <Typography styleAs="body" as="span">
              {`${
                (itemData?.price || 0) +
                currIngredients.reduce((prev, curr) => {
                  return (
                    prev +
                    Math.max(curr.count! - curr.defaultCount, 0) * curr.price
                  );
                }, 0)
              } руб.`}
            </Typography>
          </Button>

          {item.uuid === undefined && (
            <Button
              theme="primary"
              size="md"
              onClick={handleAdd}
            >
              <Typography styleAs="callout" as="span">
                Добавить в корзину
              </Typography>
            </Button>
          )}

          {item.uuid !== undefined && (
            <>
              <Button
                theme="danger"
                size="md"
                onClick={() => {
                  handleRemove();
                }}
              >
                <Typography styleAs="callout" as="span">
                  Удалить из корзины
                </Typography>
              </Button>

              {!item.fixed && (
                <Button
                  theme="primary"
                  size="md"
                  onClick={handleSave}
                >
                  <Typography styleAs="callout" as="span">
                    Сохранить
                  </Typography>
                </Button>
              )}
            </>
          )}
        </div>
      </div>

    </>
  );
};
