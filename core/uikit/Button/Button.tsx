import React, { ButtonHTMLAttributes, CSSProperties } from 'react';
import cn from 'classnames';

import styles from './Button.module.scss';
import { TFunc } from 'core/types/types';

type size = 'sm' | 'md' | 'lg';
type theme = 'primary' | 'secondary' | 'danger';
type variant = 'outline' | 'filled';

interface IButtonProps {
  size?: size;
  className?: string;
  theme?: theme;
  round?: boolean;
  style?: CSSProperties;
  variant?: variant;
  type?: 'submit' | 'reset' | 'button' | undefined;
  onClick?: TFunc;
  disabled?: boolean;
}

const Button: React.FC<IButtonProps> = ({
  children,
  size = 'md',
  className,
  theme = 'primary',
  round = false,
  variant = 'filled',
  style,
  type = 'button',
  onClick,
  disabled
}) => {
  return <button
    type={type}
    className={cn(
      [
        styles.Button,
        className
      ],
      {
        [styles[`Button--size-${size}`]]: !!size,
        [styles[`Button--theme-${theme}`]]: !!theme,
        [styles[`Button--variant-${variant}`]]: !!variant,
        [styles['Button--round']]: round,
        [styles['Button--disabled']]: disabled
      }
    )}
    style={style}
    onClick={!disabled ? onClick : undefined}
    disabled={disabled}
  >
    <div className={styles.Button__container}>
      {children}
    </div>
  </button>
}

export default Button;