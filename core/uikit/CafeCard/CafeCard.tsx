import classNames from "classnames";
import type { ICafeDTO } from "core/redux/types/dto/cafe";
import { TFunc } from "core/types/types";
import Badge from "../Badge";

import styles from './CafeCard.module.scss'

interface IProps {
    cafeInfo: ICafeDTO;
}

const CafeCard: React.FC<IProps> = ({
    cafeInfo
}) => {
    return (
        <div className={styles.cafeCard}>
            <span className={styles.cafeCard__name}>{cafeInfo.name}</span>
            {cafeInfo.closed ? (
                <Badge className={classNames(styles.cafeCard__badge, styles['cafeCard__badge--closed'])}>Закрыто</Badge>
            ) : (
                <Badge className={classNames(styles.cafeCard__badge, styles['cafeCard__badge--opened'])}>Открыто</Badge>
            )}
            <span className={styles.cafeCard__address}>{cafeInfo.address}</span>
        </div>
    )
}

export default CafeCard;