import { FunctionComponent, MouseEventHandler, ReactChild, ReactElement, useState } from "react";
import styles from './CountButton.module.scss';
import cn from 'classnames';
import { Typography } from "../Typography";

type SquarebuttonProps = {
  onAdd?: ()=>void;
  onSub?: ()=>void;
  value: number;
  title: string;
  subtitle?: string | number;
  className?: string;
  minCount?: number;
  maxCount?: number;
};

export const CountButton: FunctionComponent<SquarebuttonProps> = ({
  className,
  onAdd,
  onSub,
  value,
  title,
  subtitle,
  maxCount,
  minCount = 0
}) => {
  const classes = cn(className, styles.CountButton);
  
  const handleSub = ()=>{
    if(value > minCount){
      if(onSub) onSub()
    } 
  }

  const handleAdd = ()=>{
    if(typeof maxCount != 'undefined'){
      if(value < maxCount){
        if(onAdd) onAdd()
      }
    } else {
      if(onAdd) onAdd()
    }
  }

  return (
    <div className={classes}>
      <button className={styles.button} disabled={value === minCount} onClick={handleSub}>-</button>
      <div className={styles.main}>
        <div className={styles.info}>
          <Typography as='span' styleAs='callout' className={styles.title}>{title}</Typography>
          {subtitle && <Typography as='span' styleAs='caption-2' className={styles.subtitle}>{subtitle}</Typography>}
        </div>
        <div className={styles.count}>
          <Typography as='span' styleAs='callout'>x{value}</Typography>
        </div>
      </div>
      <button className={styles.button} disabled={value === maxCount} onClick={handleAdd}>+</button>
    </div>
  );
};
