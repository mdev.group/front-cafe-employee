import classNames from "classnames";
import { ChangeEventHandler, useCallback, useState } from "react";
import Dropdown from "../Dropdown";
import Input from "../Input/Input";

import styles from './SearchInput.module.scss';

interface IItem {
  label: string;
  onClick: () => void;
}

interface IProps {
  items: IItem[];
  placeholder?: string;
  label?: string;
  onUpdateSearch?: (value: string) => void
  className?: string;
  value?: string;
}

const SearchInput: React.FC<IProps> = ({
  label,
  onUpdateSearch,
  className,
  items,
  value: valueLabel
}) => {
  const [value, setValue] = useState('');

  const handleChange = useCallback((newValue) => {
    setValue(newValue);

    if(newValue.length >= 3) {
      if(onUpdateSearch) {
        onUpdateSearch(newValue)
      }
    }
  }, [onUpdateSearch]);

  const handleClick = useCallback((item: IItem) => () => {
    if(item.onClick) item.onClick()
    setValue('')
  }, [])

  return (
    <div className={classNames(className, styles.field)}>
      <Input
        value={valueLabel || value}
        onChange={handleChange}
        label={label}
        className={className}
      />
      
      {value.length >= 3 && items && (
        <Dropdown
          classNames={{
            main: styles.field__dropdown
          }}
          items={items.map((item) => (
            {
              onClick: handleClick(item),
              label: item.label
            }
          ))}
          onClose={() => {}}
        />
      )}
    </div>
  )
};

export default SearchInput;