import { FunctionComponent, MouseEventHandler, ReactChild, ReactElement } from "react";
import styles from './SquareButton.module.scss';
import cn from 'classnames';
import Typo from "../Typo";
import { CreateIconProps } from "core/uikit/Icon/Icon";

type SquarebuttonProps = {
  theme?: "default" | "danger" | "success";
  active?: boolean;
  borders?: boolean;
  Icon?: FunctionComponent<CreateIconProps>;
  label?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  className?: string;
  size?: 'lg' | 'md'
};

export const SquareButton: FunctionComponent<SquarebuttonProps> = ({
  active = true,
  theme = 'default',
  Icon,
  label = 'label',
  borders = false,
  onClick,
  size = 'md',
  className
}) => {
  const classes = cn(
    className,
    styles.SquareButton,
    {
      [styles.active]: active,
      [styles.borders]: borders
    },
    styles[`theme-${theme}`],
    styles[`size-${size}`]);

  return (
    <button className={classes} onClick={active ? onClick : undefined}>
      {Icon && <Icon className={styles.Icon} size='custom'/>}
      <Typo variant='title-16' className={styles.Label}>{label}</Typo>
    </button>
  );
};
