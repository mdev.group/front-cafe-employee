import { ChangeEvent, ChangeEventHandler, FunctionComponent, useRef, useState } from "react";
import styles from "./SplitNumberInput.module.scss";
import cn from "classnames";
import Typo from "../Typo";

type SplitNumberInputType = {
  sets?: Array<number> | number;
  onSend?: (value: string)=>void;
  success?: boolean | null;
};

export const SplitNumberInput: FunctionComponent<SplitNumberInputType> = ({
  sets = [0, 0, 0, 0],
  onSend,
  success = null
}) => {
  const inputRef = useRef<HTMLInputElement>(null);

  if (typeof sets == "number") {
    sets = [sets]
  }

  const [values, setValues] = useState<string>("");

  const handleFocus = (numIndex: number) => {
    const target = inputRef.current!;
    target.focus()

    setValues(values.split('').splice(0,numIndex).join(''));
  }

  const handleChange: ChangeEventHandler<HTMLInputElement> = (event)=>{
    if (typeof sets == "number") {
      sets = [sets]
    }

    const maxIndex = sets.reduce((prev, curr)=>{return prev + curr},0)

    setValues(event.target.value.split('').splice(0,maxIndex).join(''));

    if(event.target.value.length == maxIndex){
      // event.target.blur()
      handleSend(event.target.value)
    }
  }

  const handleSend = (value: string)=>{
    if(onSend) onSend(value)
  }

  var numIndex = 0;
  return (
    <div className={styles.wrapper}>
      <input ref={inputRef} value={values} onChange={handleChange} className={styles.hidden}/>
      {sets.map((group, index) => {
        return (
          <div key={index} className={styles.group}>
            {[...Array(group)].map((num, index) => {
              const numIndexx = numIndex++;
              return (
                <div key={index} className={styles.box} onClick={()=>{handleFocus(numIndexx)}}>
                  <Typo variant='title-24' className={styles.digit}>{values.split('')[numIndexx]}</Typo>
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};
