import classNames from 'classnames';
import { TFunc } from 'core/types/types';
import styles from './CreateButton.module.scss';

interface IProps {
    className?: string;
    onClick: TFunc;
}

const CreateButton: React.FC<IProps> = ({
    onClick,
    className,
    children
}) => {
    return (
        <div className={classNames(className, styles.button)} onClick={onClick}>
          <span className={styles.button__inner}>
            {children}
          </span>
        </div>
    )
}

export default CreateButton;