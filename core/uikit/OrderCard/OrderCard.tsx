import {
  ChangeEvent,
  ChangeEventHandler,
  FunctionComponent,
  useMemo,
  useRef,
  useState,
} from "react";
import styles from "./OrderCard.module.scss";
import cn from "classnames";
import { Typography } from "../Typography";
import { useSelector } from "react-redux";
import { IOrder } from "core/redux/types/dto/order";
import { selectedOrderCartTotal_selector, selectedOrderCart_selector } from "core/redux/selectors";
import { mapStatusToString } from "./mapStatusToString";

type OrderCardType = {
  order: IOrder,
  onClick?: ()=>void;
};

export const OrderCard: FunctionComponent<OrderCardType> = ({
  order,
  onClick
}) => {
  const cart = useSelector(selectedOrderCart_selector)

  const status = useMemo(() => mapStatusToString(order.status), [order]);

  return (
    <div className={styles.OrderCard} onClick={onClick}>
      <div className={styles.Row}>
        <Typography styleAs='body' color='#111'># {order.number}</Typography>

        {!order.table ? (
          <Typography styleAs='caption-1' color="#111">С собой</Typography>
        ) : (
          <div className={styles.param}>
            <Typography styleAs='caption-1' color="#111">В заведении</Typography>
            {/* <Typography styleAs='body' color="#2E78E8">{order.table}</Typography> */}
          </div>
        )}
      </div>
      <div className={styles.Row}>
        <Typography styleAs='body' color="#2E78E8">{status}</Typography>
      </div>
    </div>
  );
};
