import classNames from 'classnames';
import { TFunc } from 'core/types/types';
import { isChild } from 'core/utils/isChild';
import { useCallback, useMemo, useState } from 'react';
import Dropdown from '../Dropdown';
import styles from './Select.module.scss';

type TValue = TFunc | string | number | object;

interface IOption {
  label: string;
  value?: TValue;
  onClick?: TFunc;
}

interface IProps {
  options: IOption[];
  onSelect?: (item: IOption) => void;
  className?: string;
  label?: string;
  value?: IOption;
}

const Select: React.FC<IProps> = ({
  options,
  onSelect,
  className,
  label,
  value: controlledValue
}) => {
  const [value, setValue] = useState<IOption | null>(null);

  const fieldValue = useMemo(()=>{
    return controlledValue !== undefined ? controlledValue : value;
  }, [value, controlledValue])

  const [opened, setOpened] = useState(false);

  const handleSelect = useCallback((option: IOption) => () => {
    setOpened(false);
    setValue(option)
    if(onSelect) onSelect(option);
    if(option.onClick) option.onClick();
  }, [onSelect])

  const handleOpen = useCallback((e) => {
    if(isChild(e.target, e.currentTarget, 2)) {
      setOpened(true);
    }
  }, [])

  return (
    <div className={classNames(styles.input, className, {
      [styles.input_filled]: fieldValue,
      [styles.input_nolabel]: !label,
    })} onClick={handleOpen}>
      <span className={styles.label}>{label}</span>
      {fieldValue?.label && <span className={styles.input__value}>{fieldValue?.label}</span>}
      {opened && (
        <Dropdown
          onClose={() => {setOpened(false)}}
          classNames={{
            main: styles.input__dropdown
          }}
          items={
            options.map((item) => (
              {
                label: item.label,
                onClick: handleSelect(item)
              }
            ))
          }
        />
      )}
    </div>
  )
}

export default Select;