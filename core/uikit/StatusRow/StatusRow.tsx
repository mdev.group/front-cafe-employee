import React, {
  ChangeEvent,
  ChangeEventHandler,
  FunctionComponent,
  useState,
} from "react";
import styles from "./StatusRow.module.scss";
import cn from "classnames";
import { Typography } from "../Typography";
import { Check, Close } from "../Icon/Icons";

type StatusRowType = {
  active: boolean | null;
  label: string;
};

export const StatusRow: FunctionComponent<StatusRowType> = ({
  active = null,
  label,
  children
}) => {
  var numIndex = 0;
  return (
    <>
      <div className={cn(styles.StatusRow, active && styles.active)}>
        <Typography styleAs="body" color="#111">
          {label}
        </Typography>
        <div className={styles.Status}>
          {active !== null ? active ? <Check size='lg' color='#2E78E8'/> : <Close size='lg' color='#EC7E7E'/> : <></>}
        </div>
      </div>
      
      {active && children}
      
    </>
  );
};

type StatusItemType = {
  render?: boolean;
};

export const StatusItem: FunctionComponent<StatusItemType> = ({
  render = false,
  children
}) => {
  var numIndex = 0;
  return (
    <>
      {render && children}
    </>
  );
};