import { StatusRow, StatusItem } from './StatusRow';

export default {
  Row: StatusRow,
  Item: StatusItem
}