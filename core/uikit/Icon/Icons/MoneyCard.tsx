import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const CreditCard: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <>
    <g clipPath="url(#prefix__clip0_122_7268)">
        <mask
          id="prefix__a"
          style={{
            maskType: "alpha",
          }}
          maskUnits="userSpaceOnUse"
          x={0}
          y={3}
          width={13}
          height={20}
        >
          <path d="M6.5 22.5H0v-19L13 3 6.5 22.5z" />
        </mask>
        <g mask="url(#prefix__a)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M4 6a1 1 0 00-1 1v.828A3 3 0 004.828 6H4zm0-2a3 3 0 00-3 3v10a3 3 0 003 3h16a3 3 0 003-3V7a3 3 0 00-3-3H4zm2.899 2A5 5 0 013 9.899V17a1 1 0 001 1h13.101A5 5 0 0121 14.101V7a1 1 0 00-1-1H6.899zM21 16.172A2.999 2.999 0 0019.172 18H20a1 1 0 001-1v-.828zM12 10a2 2 0 100 4 2 2 0 000-4zm-4 2a4 4 0 118 0 4 4 0 01-8 0z"
          />
        </g>
        <mask
          id="prefix__b"
          style={{
            maskType: "alpha",
          }}
          maskUnits="userSpaceOnUse"
          x={11}
          y={2}
          width={13}
          height={20}
        >
          <path d="M17.5 2h6v19l-12.5.5L17.5 2z" fill="#C4C4C4" />
        </mask>
        <g
          mask="url(#prefix__b)"
          fillRule="evenodd"
          clipRule="evenodd"
        >
          <path d="M4.875 5.25C3.839 5.25 3 6.09 3 7.125v9.75c0 1.035.84 1.875 1.875 1.875h14.25c1.035 0 1.875-.84 1.875-1.875v-9.75c0-1.036-.84-1.875-1.875-1.875H4.875zM1.5 7.125A3.375 3.375 0 014.875 3.75h14.25A3.375 3.375 0 0122.5 7.125v9.75a3.375 3.375 0 01-3.375 3.375H4.875A3.375 3.375 0 011.5 16.875v-9.75z" />
          <path d="M21.75 10.5H2.25v-3h19.5v3zM5 14.063a1 1 0 011-1h2.25a1 1 0 011 1V15a1 1 0 01-1 1H6a1 1 0 01-1-1v-.938z" />
        </g>
        <rect
          x={15.077}
          y={-0.214}
          width={2}
          height={25.083}
          rx={1}
          transform="rotate(18.362 15.077 -.214)"
        />
      </g>
      <defs>
        <clipPath id="prefix__clip0_122_7268">
          <path d="M0 0h24v24H0z" />
        </clipPath>
      </defs>
    </>
  </Icon>
));

export default CreditCard;
