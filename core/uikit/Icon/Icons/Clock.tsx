import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const QrCode: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <path d="M12 3a9 9 0 100 18 9 9 0 000-18zM1 12C1 5.925 5.925 1 12 1s11 4.925 11 11-4.925 11-11 11S1 18.075 1 12z" />
    <path d="M12 5a1 1 0 011 1v6.172a1 1 0 00.293.707l2.414 2.414a1 1 0 01-1.414 1.414l-2.414-2.414a3 3 0 01-.879-2.12V6a1 1 0 011-1z" />
  </Icon>
));

export default QrCode;
