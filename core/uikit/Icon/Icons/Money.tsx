import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const CreditCard: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4 6a1 1 0 00-1 1v.828A3 3 0 004.828 6H4zm0-2a3 3 0 00-3 3v10a3 3 0 003 3h16a3 3 0 003-3V7a3 3 0 00-3-3H4zm2.899 2A5 5 0 013 9.899V17a1 1 0 001 1h13.101A5 5 0 0121 14.101V7a1 1 0 00-1-1H6.899zM21 16.172A2.999 2.999 0 0019.172 18H20a1 1 0 001-1v-.828zM12 10a2 2 0 100 4 2 2 0 000-4zm-4 2a4 4 0 118 0 4 4 0 01-8 0z"
      />
    </>
  </Icon>
));

export default CreditCard;
