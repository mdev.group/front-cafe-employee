import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const QrCode: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <path d="M17.268 4.526L5.04 16.756a1 1 0 00-.273.51l-.492 2.46 2.458-.493a1 1 0 00.51-.273L19.475 6.73a1.56 1.56 0 00-2.206-2.204zM3.626 15.34L15.863 3.103l.016-.016a3.56 3.56 0 015.034 5.033l-.016.016L8.658 20.374a3 3 0 01-1.533.82l-3.929.787a1 1 0 01-1.177-1.177l.786-3.93a3 3 0 01.82-1.533z" />
    <path d="M16.793 10.207l-3-3 1.414-1.414 3 3-1.414 1.414z" />
  </Icon>
));

export default QrCode;
