import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const CreditCard: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <>
      <path
        d="M4.875 5.25C3.839 5.25 3 6.09 3 7.125v9.75c0 1.035.84 1.875 1.875 1.875h14.25c1.035 0 1.875-.84 1.875-1.875v-9.75c0-1.036-.84-1.875-1.875-1.875H4.875zM1.5 7.125A3.375 3.375 0 014.875 3.75h14.25A3.375 3.375 0 0122.5 7.125v9.75a3.375 3.375 0 01-3.375 3.375H4.875A3.375 3.375 0 011.5 16.875v-9.75z"
      />
      <path
        d="M21.75 10.5H2.25v-3h19.5v3zM5 14.063a1 1 0 011-1h2.25a1 1 0 011 1V15a1 1 0 01-1 1H6a1 1 0 01-1-1v-.938z"
      />
    </>
  </Icon>
));

export default CreditCard;
