import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const QrCode: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <path d="M20.64 5.232a1 1 0 01.128 1.408l-10 12a1 1 0 01-1.475.067l-6-6a1 1 0 111.414-1.414l5.226 5.226 9.299-11.16a1 1 0 011.408-.127z" />
  </Icon>
));

export default QrCode;
