import QrCode from './QrCode';
import Pen from './Pen';
import Check from './Check';
import Close from './Close';
import Plus from './Plus';
import Clock from './Clock';
import Exit from './Exit';
import CreditCard from './CreditCard';
import Money from './Money';
import MoneyCard from './MoneyCard';

export {
  QrCode,
  Pen,
  Plus,
  Close,
  Check,
  Clock,
  Exit,
  CreditCard,
  Money,
  MoneyCard
}