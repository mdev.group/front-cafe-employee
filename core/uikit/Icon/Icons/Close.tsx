import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const QrCode: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <path d="M20 5.414L13.414 12 20 18.586A1 1 0 0118.586 20L12 13.414 5.414 20A1 1 0 014 18.586L10.586 12 4 5.414A1 1 0 015.414 4L12 10.586 18.586 4A1 1 0 1120 5.414z" />
  </Icon>
));

export default QrCode;
