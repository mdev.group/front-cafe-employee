import React, { forwardRef, FunctionComponent } from "react";
import { Icon, CreateIconProps } from "../Icon";

const QrCode: FunctionComponent<CreateIconProps> = forwardRef((props, ref) => (
  <Icon {...props} ref={ref}>
    <path d="M18.027 7.232a1 1 0 011.408.128l3.322 3.986A.989.989 0 0123 12a1.003 1.003 0 01-.243.654l-3.322 3.986a1 1 0 11-1.536-1.28L19.865 13H13a1 1 0 110-2h6.865L17.9 8.64a1 1 0 01.128-1.408z" />
    <path d="M11.751 2.184A3 3 0 0115 5.174V7a1 1 0 11-2 0V5.174a1 1 0 00-1.083-.996l-8 .666A1 1 0 003 5.84m8.751-3.656l-8 .666A3 3 0 001 5.84v12.32a3 3 0 002.751 2.99l8 .667A3 3 0 0015 18.826V17a1 1 0 10-2 0v1.826a1.002 1.002 0 01-1.083.997l-8-.667A1 1 0 013 18.16V5.84" />
  </Icon>
));

export default QrCode;
