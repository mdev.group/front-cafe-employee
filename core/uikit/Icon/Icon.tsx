import React, { ReactChild, useEffect, useRef } from 'react';
import cn from 'classnames';
import styles from './Icon.module.scss';

export type Color = string | {
  default: string,
  hover?: string,
  active?: string
} | undefined;


export type IconProps = {
  className?: string;
  size?: 'sm' | 'md' | 'lg' | 'xl' | 'custom';
  color?: Color;
  viewBox?: string,
  children: ReactChild | ReactChild[]
};


export type CreateIconProps = Omit<IconProps, 'children'>


const defaultViewBox: string = '0 0 24 24';

const getSize = (viewBox: string) => {
  const [, , width, height] = viewBox.split(' ');
  return {
    width,
    height,
  };
};

const getColor = (color: string) => {
  if(color[0] == '#'){
    return color;
  } else if(color.indexOf('var') == 0){
    return color;
  }
};

function setColors(color: Color, ref: any){
  if(ref.current){
    if(typeof color == 'object'){
      ref.current.style.setProperty('--icon-color', getColor(color.default))
      ref.current.style.setProperty('--icon-color--hover',getColor(color.hover || color.default))
      ref.current.style.setProperty('--icon-color--focus',getColor(color.active || color.default))
    } else if (typeof color == 'string'){
      ref.current.style.setProperty('--icon-color', getColor(color))
      ref.current.style.setProperty('--icon-color--hover',getColor(color))
      ref.current.style.setProperty('--icon-color--focus',getColor(color))
    } else {
      ref.current.style.setProperty('--icon-color--hover', 'var(--icon-color)')
      ref.current.style.setProperty('--icon-color--focus', 'var(--icon-color)')
    }
  }
}





export const Icon = React.forwardRef(({
  className,
  size = 'md',
  color = 'var',
  viewBox = defaultViewBox,
  children
}: IconProps, ref) => {
  const iconRef = useRef<SVGSVGElement>(null);
  const classes = cn(className, styles.icon, styles.color_custom, {
    [styles[`size-${size}`]]: viewBox === defaultViewBox
  });

  useEffect(()=>{
    setColors(color, iconRef);
  }, [ref])

  return (
    <svg
      ref={iconRef}
      className={classes}
      focusable="false"
      viewBox={viewBox}
      {...getSize(viewBox)}
    >
      {children}
    </svg>
  );
});
