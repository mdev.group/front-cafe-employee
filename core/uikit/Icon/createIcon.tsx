import React, {
  ReactElement,
  FunctionComponent,
} from "react";
import { Icon } from "./Icon";

type PathsProps = ReactElement[] | ReactElement

export const createIcon: Function = (pathes: PathsProps) => {
  if (!Array.isArray(pathes)) {
    pathes = [pathes];
  }

  return (
    <Icon>
      {(pathes as ReactElement[]).map((path, index) => {
        return React.cloneElement(path, {
          key: index,
        });
      })}
    </Icon>
  );
};
