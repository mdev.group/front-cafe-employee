https://www.figma.com/file/VvjjHeiJkbv1cV0eULG6Oy/VTB-%E2%86%92-Design-Guidelines-vtb.ru-2.0?node-id=8436%3A41253

#### Размеры

```jsx noeditor
import LockFilledIcon from './icons/LockFilledIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <LockFilledIcon />
      <div className="icon-name">small</div>
    </div>
    <div className="icon-box">
      <LockFilledIcon size="normal" />
      <div className="icon-name">normal</div>
    </div>
    <div className="icon-box">
      <LockFilledIcon size="large" />
      <div className="icon-name">large</div>
    </div>
  </div>
</div>;
```

#### Навигационные иконки

```jsx noeditor
import ArrowDiagIcon from './icons/ArrowDiagIcon';
import ArrowDownIcon from './icons/ArrowDownIcon';
import ArrowLeftIcon from './icons/ArrowLeftIcon';
import ArrowRightIcon from './icons/ArrowRightIcon';
import ArrowRightLongIcon from './icons/ArrowRightLongIcon';
import ArrowUpIcon from './icons/ArrowUpIcon';
import ChevronLeftIcon from './icons/ChevronLeftIcon';
import ChevronRightIcon from './icons/ChevronRightIcon';
import CloseIcon from './icons/CloseIcon';
import MenuIcon from './icons/MenuIcon';
import MinusIcon from './icons/MinusIcon';
import PlusIcon from './icons/PlusIcon';
import SearchIcon from './icons/SearchIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <MenuIcon />
      <div className="icon-name">Menu</div>
    </div>
    <div className="icon-box">
      <CloseIcon />
      <div className="icon-name">Close</div>
    </div>
    <div className="icon-box">
      <SearchIcon />
      <div className="icon-name">Search</div>
    </div>
    <div className="icon-box">
      <ArrowRightIcon />
      <div className="icon-name">ArrowRight</div>
    </div>
    <div className="icon-box">
      <ArrowLeftIcon />
      <div className="icon-name">ArrowLeft</div>
    </div>
    <div className="icon-box">
      <ArrowDownIcon />
      <div className="icon-name">ArrowDown</div>
    </div>
    <div className="icon-box">
      <ArrowUpIcon />
      <div className="icon-name">ArrowUp</div>
    </div>
    <div className="icon-box">
      <ArrowDiagIcon />
      <div className="icon-name">ArrowDiag</div>
    </div>
    <div className="icon-box">
      <ChevronLeftIcon />
      <div className="icon-name">ChevronLeft</div>
    </div>
    <div className="icon-box">
      <ChevronRightIcon />
      <div className="icon-name">ChevronRight</div>
    </div>
    <div className="icon-box">
      <MinusIcon />
      <div className="icon-name">Minus</div>
    </div>
    <div className="icon-box">
      <PlusIcon />
      <div className="icon-name">Plus</div>
    </div>
  </div>
</div>;
```

#### Выпадающие списки

```jsx noeditor
import ChevronDownIcon from './icons/ChevronDownIcon';
import ChevronUpIcon from './icons/ChevronUpIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <ChevronDownIcon />
      <div className="icon-name">ChevronDown</div>
    </div>
    <div className="icon-box">
      <ChevronUpIcon />
      <div className="icon-name">ChevronUp</div>
    </div>
  </div>
</div>;
```

#### Социальные сети

```jsx noeditor
import FacebookIcon from './icons/FacebookIcon';
import InstagramIcon from './icons/InstagramIcon';
import OKIcon from './icons/OKIcon';
import TwitterIcon from './icons/TwitterIcon';
import VKIcon from './icons/VKIcon';
import YouTubeIcon from './icons/YouTubeIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <InstagramIcon />
      <div className="icon-name">Instagram</div>
    </div>
    <div className="icon-box">
      <FacebookIcon />
      <div className="icon-name">Facebook</div>
    </div>
    <div className="icon-box">
      <YouTubeIcon />
      <div className="icon-name">YouTube</div>
    </div>
    <div className="icon-box">
      <VKIcon />
      <div className="icon-name">VK</div>
    </div>
    <div className="icon-box">
      <TwitterIcon />
      <div className="icon-name">Twitter</div>
    </div>
    <div className="icon-box">
      <OKIcon />
      <div className="icon-name">OK</div>
    </div>
  </div>
</div>;
```

#### Уведомления

```jsx noeditor
import ErrorFilledIcon from './icons/ErrorFilledIcon';
import ErrorOutlineIcon from './icons/ErrorOutlineIcon';
import HelpIcon from './icons/HelpIcon';
import InfoFilledIcon from './icons/InfoFilledIcon';
import InfoOutlineIcon from './icons/InfoOutlineIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <HelpIcon />
      <div className="icon-name">Help</div>
    </div>
    <div className="icon-box">
      <InfoOutlineIcon />
      <div className="icon-name">InfoOutline</div>
    </div>
    <div className="icon-box">
      <InfoOutlineIcon />
      <div className="icon-name">InfoOutline</div>
    </div>
    <div className="icon-box">
      <ErrorOutlineIcon />
      <div className="icon-name">ErrorOutline</div>
    </div>
    <div className="icon-box">
      <ErrorFilledIcon />
      <div className="icon-name">ErrorFilled</div>
    </div>
  </div>
</div>;
```

#### Доступность

```jsx noeditor
import VisibleOffIcon from './icons/VisibleOffIcon';
import VisibleOnIcon from './icons/VisibleOnIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <VisibleOnIcon />
      <div className="icon-name">VisibleOn</div>
    </div>
    <div className="icon-box">
      <VisibleOffIcon />
      <div className="icon-name">VisibleOffIcon</div>
    </div>
  </div>
</div>;
```

#### Приложения

```jsx noeditor
import AppleIcon from './icons/AppleIcon';
import GooglePlayIcon from './icons/GooglePlayIcon';
import HuaweiIcon from './icons/HuaweiIcon';
import VTBOnlineIcon from './icons/VTBOnlineIcon';
import WindowsIcon from './icons/WindowsIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <HuaweiIcon />
      <div className="icon-name">Huawei</div>
    </div>
    <div className="icon-box">
      <AppleIcon />
      <div className="icon-name">Apple</div>
    </div>
    <div className="icon-box">
      <GooglePlayIcon />
      <div className="icon-name">GooglePlay</div>
    </div>
    <div className="icon-box">
      <WindowsIcon />
      <div className="icon-name">Windows</div>
    </div>
    <div className="icon-box">
      <VTBOnlineIcon />
      <div className="icon-name">VTBOnline</div>
    </div>
  </div>
</div>;
```

#### Документы

```jsx noeditor
import DOCFileIcon from './icons/DOCFileIcon';
import PDFFileIcon from './icons/PDFFileIcon';
import PPTFileIcon from './icons/PPTFileIcon';
import RARFileIcon from './icons/RARFileIcon';
import XLSFileIcon from './icons/XLSFileIcon';
import XMLFileIcon from './icons/XMLFileIcon';
import ZIPFileIcon from './icons/ZIPFileIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <DOCFileIcon size="large" />
      <div className="icon-name">DOCFile</div>
    </div>
    <div className="icon-box">
      <PDFFileIcon size="large" />
      <div className="icon-name">PDFFile</div>
    </div>
    <div className="icon-box">
      <PPTFileIcon size="large" />
      <div className="icon-name">PPTFile</div>
    </div>
    <div className="icon-box">
      <RARFileIcon size="large" />
      <div className="icon-name">RARFile</div>
    </div>
    <div className="icon-box">
      <XLSFileIcon size="large" />
      <div className="icon-name">XLSFile</div>
    </div>
    <div className="icon-box">
      <XMLFileIcon size="large" />
      <div className="icon-name">XMLFile</div>
    </div>
    <div className="icon-box">
      <ZIPFileIcon size="large" />
      <div className="icon-name">ZIPFile</div>
    </div>
  </div>
</div>;
```

#### Другое

```jsx noeditor
import CalendarIcon from './icons/CalendarIcon';
import CallIcon from './icons/CallIcon';
import DownloadIcon from './icons/DownloadIcon';
import GeoIcon from './icons/GeoIcon';
import GosuslugiIcon from './icons/GosuslugiIcon';
import LockFilledIcon from './icons/LockFilledIcon';
import LockOutlineIcon from './icons/LockOutlineIcon';
import LoginIcon from './icons/LoginIcon';
import PrintIcon from './icons/PrintIcon';
import RussiaFlagIcon from './icons/RussiaFlagIcon';
import UKFlagIcon from './icons/UKFlagIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <CalendarIcon />
      <div className="icon-name">Calendar</div>
    </div>
    <div className="icon-box">
      <CallIcon />
      <div className="icon-name">Call</div>
    </div>
    <div className="icon-box">
      <DownloadIcon />
      <div className="icon-name">Download</div>
    </div>
    <div className="icon-box">
      <GeoIcon />
      <div className="icon-name">Geo</div>
    </div>
    <div className="icon-box">
      <GosuslugiIcon />
      <div className="icon-name">Gosuslugi</div>
    </div>
    <div className="icon-box">
      <LockFilledIcon />
      <div className="icon-name">LockFilled</div>
    </div>
    <div className="icon-box">
      <LockOutlineIcon />
      <div className="icon-name">LockOutline</div>
    </div>
    <div className="icon-box">
      <LoginIcon />
      <div className="icon-name">Login</div>
    </div>
    <div className="icon-box">
      <PrintIcon />
      <div className="icon-name">Print</div>
    </div>
    <div className="icon-box">
      <RussiaFlagIcon />
      <div className="icon-name">RussiaFlag</div>
    </div>
    <div className="icon-box">
      <UKFlagIcon />
      <div className="icon-name">UKFlag</div>
    </div>
  </div>
</div>;
```

#### Внутри кнопки

```jsx noeditor
import ArrowLeftLongIcon from './icons/ArrowLeftLongIcon';
import ArrowRightLongIcon from './icons/ArrowRightLongIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box">
      <ArrowLeftLongIcon />
      <div className="icon-name">ArrowLeftLong</div>
    </div>
    <div className="icon-box">
      <ArrowLeftLongIcon size="large" />
      <div className="icon-name">ArrowLeftLong</div>
    </div>
    <div className="icon-box">
      <ArrowRightLongIcon size="large" />
      <div className="icon-name">ArrowRightLong</div>
    </div>
    <div className="icon-box">
      <ArrowRightLongIcon />
      <div className="icon-name">ArrowRightLong</div>
    </div>
  </div>
</div>;
```

#### Кастомизация

```jsx noeditor
import LockFilledIcon from './icons/LockFilledIcon';

<div className="examples">
  <div className="icon-container">
    <LockFilledIcon />
    <LockFilledIcon color="blue" />
    <LockFilledIcon htmlColor="cyan" size="normal" />
    <LockFilledIcon htmlColor="red" size="large" />
  </div>
</div>;
```

#### Иконки невписывающиеся в стандартные размеры сетки(24, 32, 48). Не злоупотреблять!

```jsx noeditor
import ChevronDownLongIcon from './icons/ChevronDownLongIcon';
import LineIcon from './icons/LineIcon';
import CheckIcon from './icons/CheckIcon';
import AppleIconWithText from './icons/AppleIconWithText';
import GooglePlayIconWithText from './icons/GooglePlayIconWithText';
import HuaweiIconWithText from './icons/HuaweiIconWithText';
import LogoIcon from './icons/LogoIcon';

<div className="examples">
  <div className="icon-container">
    <div className="icon-box icon-box--wide">
      <ChevronDownLongIcon viewBox="0 0 35 10" />
      <div className="icon-name">ChevronDownLongIcon</div>
    </div>
    <div className="icon-box icon-box--wide">
      <LineIcon viewBox="0 0 35 4" />
      <div className="icon-name">LineIcon</div>
    </div>
    <div className="icon-box icon-box--wide">
      <CheckIcon viewBox="0 0 12 12" />
      <div className="icon-name">CheckIcon</div>
    </div>
    <div className="icon-box icon-box--wide">
      <HuaweiIconWithText viewBox="0 0 124 32" />
      <div className="icon-name">HuaweiIconWithText</div>
    </div>
    <div className="icon-box icon-box--wide">
      <AppleIconWithText viewBox="0 0 124 32" />
      <div className="icon-name">AppleIconWithText</div>
    </div>
    <div className="icon-box icon-box--wide">
      <GooglePlayIconWithText viewBox="0 0 124 32" />
      <div className="icon-name">GooglePlayIconWithText</div>
    </div>
    <div className="icon-box icon-box--wide">
      <LogoIcon viewBox="0 0 98 35" />
      <div className="icon-name">LogoIcon</div>
    </div>
  </div>
</div>;
```
