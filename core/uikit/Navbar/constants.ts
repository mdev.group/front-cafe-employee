import type { IChild } from "./components/Navitem/Navitem";

export const navigation = (currUrl: string): IChild[] => [
  {
    label: 'Новый заказ',
    condition: true,
    url: '/panel/editor',
    image: '/images/navbar/add.svg'
  },
  {
    label: 'Заказы',
    condition: true,
    url: '/panel/orders',
    image: '/images/navbar/clock.svg'
  }
];