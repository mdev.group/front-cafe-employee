import { useCallback, useEffect, useMemo, useState } from "react";
import Image from "core/uikit/Image";
import Navitem from "./components/Navitem";

import { navigation } from "./constants";

import styles from "./Navbar.module.scss";
import { useDispatch, useSelector } from "react-redux";
import redirectTo from "core/utils/redirectTo";
import { getUserData } from "core/redux/actions/user";
import authTokenService from "core/services/authTokenService";
import Userimage from "../Userimage";
import { useRouter } from "next/router";
import { user_selector } from "core/redux/selectors";

const Navbar: React.FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const userdata = useSelector(user_selector);

  useEffect(()=>{
    if(!authTokenService.hasTokenInLocalstorage()) {
      redirectTo('/auth');
    }
    dispatch(getUserData());
  }, [dispatch])

  const handleGoToAccount = ()=>{
    authTokenService.clearRefreshData();
    authTokenService.clearToken();
    redirectTo('/auth');
  }

  const isLoaded = useMemo(()=>{
    return [userdata].every(item=>item !== undefined);
  }, [userdata])

  return (
    <nav className={styles.navbar}>
      {isLoaded ? (
        <div className={styles.navbar__account} onClick={handleGoToAccount}>
          <Userimage userData={userdata} className={styles.navbar__accountImg} />
          <span className={styles.navbar__accountUsername}>{`${userdata?.first_name.trim()}`}</span>
        </div>
      ) : (
        <div className={styles.navbar__account} onClick={handleGoToAccount}>
          <Image className={styles.navbar__loader} src="/images/icons/loading.svg" alt="loading..."/>
        </div>
      )}

      <div className={styles.navbar__container}>
        {navigation(router.asPath).map((item, index)=>{
          return (
          <Navitem
            key={index}
            {...item}
          >
            {item.label}
          </Navitem>
          )
        })}
      </div>
    </nav>
  )
}

export default Navbar;