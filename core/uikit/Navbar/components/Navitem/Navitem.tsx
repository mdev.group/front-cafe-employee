import React, { useCallback, useEffect, useMemo, useState } from "react";
import classNames from "classnames";
import AnimateHeight from 'react-animate-height';

import styles from './Navitem.module.scss';
import { useRouter } from "next/router";
import Image from "core/uikit/Image";

export interface IChild {
  condition?: boolean;
  label: string;
  items?: IChild[];
  url?: string;
  image: string;
}

interface IProps {
  condition?: boolean;
  items?: IChild[];
  url?: string;
  className?: string;
  image: string;
}

const Navitem: React.FC<IProps> = ({
  children,
  condition = true,
  items = [],
  url,
  className,
  image
})=>{
  const router = useRouter();

  const handleOpen = useCallback(()=>{
    if(url) router.push(url);
  }, [router, url])

  if(typeof condition == 'boolean') {
    if(!condition) {
      return null;
    }
  }

  return (
    <div
      className={classNames(className, styles.navitem, {
        [styles['navitem--active']]: url && router.asPath.startsWith(url)
      })}
      onClick={(e)=>{e.stopPropagation();handleOpen()}}
    >
      <Image
        src={image}
        alt=""
        className={styles.navitem__image}
      />
      <span>{children}</span>
    </div>
  );
}

export default Navitem;