import styles from './PageRow.module.scss';

const PageRow: React.FC = ({
  children
}) => {
  return (
    <div className={styles.pageRow}>
      {children}
    </div>
  )
}

export default PageRow;