import classNames from "classnames";
import Image from "core/uikit/Image";
import styles from "./PanelContainer.module.scss";

interface IProps {
  loading: boolean;
  className?: string;
  hasCart?: boolean;
}

const Userimage: React.FC<IProps> = ({
  children,
  loading,
  className,
  hasCart = true
}) => { 
  return (
    <main className={classNames(styles.panelcontainer, className, {
      [styles['panelcontainer--full']]: !hasCart
    })}>
      {loading ? <Image className={styles.panelcontainer__loading} src="/images/icons/loading.svg" alt=""/> : children}
    </main>
  )
}

export default Userimage;