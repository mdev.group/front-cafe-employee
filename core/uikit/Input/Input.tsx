import classNames from 'classnames';
import { useControlledValue } from 'core/utils/useControlledValue';
import moment from 'moment';
import { ChangeEventHandler, useCallback, useMemo } from 'react';
import Typo from '../Typo';

import styles from './Input.module.scss';

interface IProps {
  value?: any;
  onChange?: (newValue: any) => void;
  label?: string;
  className?: string;
  placeholder?: string;
  isPassword?: boolean;
  type?: string;
  disabled?: boolean;
}

const Input: React.FC<IProps> = ({
  value: controlledValue,
  onChange,
  label,
  className,
  placeholder,
  isPassword,
  type,
  disabled
}) => {
  const [value, setValue, isControlled] = useControlledValue(controlledValue)

  const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback(({ currentTarget }) => {
    let newValue: any = currentTarget.value;

    if(type === 'date') {
      newValue = moment(newValue).unix() * 1000
    }


    if(!isControlled) {
      setValue(newValue)
    }
    if (onChange) {
      onChange(newValue.toString());
    }
  }, [onChange, isControlled, setValue, type])

  const formattedValue = useMemo(() => {
    if (type === 'date') {
      return moment(value).format("YYYY-MM-DD")
    }
    return value
  }, [value, type])


  return (
    <label className={classNames(className, styles.field, {
      [styles['field--disabled']]: disabled
    })}>
      {label && <Typo variant='rubrik' className={styles.field__label}>{label}</Typo>}
      <input
        value={formattedValue}
        onChange={handleChange}
        className={classNames(styles.field__input)}
        placeholder={placeholder}
        type={isPassword ? 'password' : (type || 'text')}
      />
    </label>
  )
}

export default Input;