import {
  FunctionComponent,
  MouseEventHandler,
  PointerEventHandler,
  ReactNode,
  useCallback,
  useState,
} from "react";
import styles from "./Item.module.scss";
import cn from "classnames";
import * as ReactDOM from "react-dom";
import { Typography } from "../Typography";
import { usePortal } from "../../hooks/usePortal";
import { ExtendItemCard } from "../ItemCard/ExtendItemCard";
import { useDispatch, useSelector } from "react-redux";
import { cafeItemByID_selector, cafe_selector, selectedOrder_selector } from "core/redux/selectors";
import { removeFromCart } from "core/redux/actions/cafe";
import { IOrderPosition } from "core/redux/types/dto/order";
import ModalContainer from "../ModalContainer";

type ItemProps = {
  className?: string;
  item: IOrderPosition;
};

const Modal = ({ children }: { children: ReactNode }) => {
  const target = usePortal("overlay-portal");
  return ReactDOM.createPortal(children, target);
};

export const Item: FunctionComponent<ItemProps> = ({
  className,
  item
}) => {
  const dispatch = useDispatch();
  const [extend, setExtend] = useState(false);

  const itemData = useSelector(cafeItemByID_selector(item.itemID))!;

  const handleClick = useCallback(() => {
    setExtend(true);
  }, []);

  const handleRemove = useCallback(() => {
    dispatch(removeFromCart(item.uuid))
  }, [dispatch, item]);

  const handleMouseDown: MouseEventHandler<HTMLElement> = useCallback((event) => {
    const target = event.currentTarget;
    const xPosStart = event.clientX;
    var xLastPos = event.clientX;
    var Grabbed = true;

    const moveCb = (event: MouseEvent) => {
      if (Grabbed) {
        xLastPos = event.x;
        target.style.transform = `translateX(${
          xLastPos - xPosStart < 0
            ? Math.max(-52, xLastPos - xPosStart)
            : (xLastPos - xPosStart) / 50
        }px)`;
      }
    };

    target.addEventListener("mousemove", moveCb);

    window.addEventListener(
      "mouseup",
      (event: MouseEvent) => {
        Grabbed = false;
        target.removeEventListener("mousemove", moveCb);

        if (xLastPos - xPosStart + 48 < 0) {
          target.parentElement!.style.transform = `translateX(calc(-100% - 24px))`;

          setTimeout(() => {
            handleRemove();
          }, 350);
        } else {
          target.style.transform = `translateX(0)`;

          if (xLastPos - xPosStart + 2 > 0) {
            shortClickCb();
          }
        }
      },
      {
        once: true,
      }
    );

    var shortClickCb = () => {
      window.clearTimeout(longClickTimer);
      handleClick();
    };

    var longClickTimer = window.setTimeout(() => {
      shortClickCb = () => {};
    }, 500);
  }, [handleClick, handleRemove]);

  const handlePointerDown: PointerEventHandler<HTMLElement> = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const target = event.currentTarget;
    const xPosStart = event.clientX;
    const yPosStart = event.clientY;
    var xLastPos = event.clientX;
    var yLastPos = event.clientY;
    var Grabbed = true;

    const moveCb = (event: PointerEvent) => {
      if (Grabbed) {
        xLastPos = event.clientX;
        yLastPos = event.clientY;
        target.style.transform = `translateX(${
          xLastPos - xPosStart < 0
            ? Math.max(-52, xLastPos - xPosStart)
            : (xLastPos - xPosStart) / 50
        }px)`;
      }
    };

    target.addEventListener("pointermove", moveCb);


    function pointerEnd(event: PointerEvent){
      Grabbed = false;
        target.removeEventListener("pointermove", moveCb);

        if (xLastPos - xPosStart + 48 < 0) {
          target.parentElement!.style.transform = `translateX(calc(-100% - 24px))`;

          setTimeout(() => {
            handleRemove();
          }, 350);
        } else {
          target.style.transform = `translateX(0)`;

          if (Math.abs(xLastPos - xPosStart) < 6 && Math.abs(yLastPos - yPosStart) < 6) {
            shortClickCb();
          }
        }
    }

    window.addEventListener(
      'pointercancel',
      ()=>{
        target.style.transform = `translateX(0)`;
      },
      {
        once: true,
      }
    );

    window.addEventListener(
      'pointerup',
      pointerEnd,
      {
        once: true,
      }
    );

    var shortClickCb = () => {
      window.clearTimeout(longClickTimer);
      handleClick();
    };

    var longClickTimer = window.setTimeout(() => {
      shortClickCb = () => {};
    }, 500);
  };

  return (
    <>
      {extend && (
        <ModalContainer onClose={() => setExtend(false)}>
          <ExtendItemCard
            item={item}
            
            onClose={() => {
              setExtend(false);
            }}
          />
        </ModalContainer>
      )}
      <div className={styles.itemSpace}>
        <div
          onMouseDown={!item.fixed ? handleMouseDown : undefined}
          onPointerDown={!item.fixed ? handlePointerDown : undefined}
          onClick={item.fixed ? handleClick : undefined}
          className={cn(styles.Item, item.fixed ? styles.locked : undefined)}
        >
          <div className={styles.itemInfo}>
            <Typography styleAs="body" as="span">
              {itemData.name}
            </Typography>
            <Typography as="span" styleAs="body" color="#2E78E8">{`${
              itemData.price
            } руб.`}</Typography>
          </div>

          {item.ingredients.filter((ing: any) => ing.count != ing.defaultCount)
            .length > 0 && (
            <div className={styles.ingredients}>
              {item.ingredients
                .filter((ing: any) => ing.count != ing.defaultCount)
                .sort((a, b) => b.count! - a.count!)
                .map((ing) => {
                  return (
                    <div key={ing.id} className={styles.ingredient}>
                      <div className={styles.ingInfo}>
                        <Typography
                          className={styles.ingCount}
                          as="span"
                          styleAs="body"
                          color={
                            ing.count! - ing.defaultCount < 0
                              ? "#E17E7E"
                              : "#7CB0FE"
                          }
                        >
                          {ing.count! - ing.defaultCount > 0
                            ? "x" + (ing.count)
                            : "-"}
                        </Typography>
                        <Typography
                          className={styles.ingName}
                          as="span"
                          styleAs="body"
                          color={
                            ing.count! - ing.defaultCount < 0
                              ? "#E17E7E"
                              : "#000"
                          }
                        >
                          {ing.name}
                        </Typography>
                      </div>

                      {Math.max(ing.count! - ing.defaultCount, 0) * ing.price !=
                        0 && (
                        <Typography as="span" styleAs="body" color="#7CB0FE">
                          {Math.max(ing.count! - ing.defaultCount, 0) *
                            ing.price}{" "}
                          руб.
                        </Typography>
                      )}
                    </div>
                  );
                })}
            </div>
          )}
        </div>
      </div>
    </>
  );
};
