import { FunctionComponent, useCallback, useMemo, useState } from "react";
import styles from "./Cart.module.scss";
import cn from "classnames";

import { useDispatch, useSelector } from "react-redux";

import { Category } from "./Category";
import { Typography } from "../Typography";
import SquareButton from "../SquareButton";
import { Check, Close as CloseIcon, Plus } from "../Icon/Icons";
import { selectedOrderCartTotal_selector, selectedOrderCart_selector, selectedOrder_selector } from "core/redux/selectors";
import { removeFromCart, updateOrderStatus, updateOrderSuccess } from "core/redux/actions/cafe";
import { EOrderStatus } from "core/redux/types/dto/order";
import redirectTo from "core/utils/redirectTo";

type CartProps = {
  className?: string;
  Close?: boolean;
  Clear?: boolean;
  Send?: boolean;
  Edit?: boolean;
};

export const Cart: FunctionComponent<CartProps> = ({
  className,
  Close,
  Clear,
  Send,
  Edit
}) => {
  const dispatch = useDispatch();
  const [cartOpened, setCartOpened] = useState(false);

  const order = useSelector(selectedOrder_selector);
  const cart = useSelector(selectedOrderCart_selector);
  const cartTotal = useSelector(selectedOrderCartTotal_selector);

  const handleClearOrder = useCallback(() => {
    dispatch(removeFromCart('all'))
  }, [dispatch])

  const handleSendOrder = useCallback(() => {
    if(order?.status === EOrderStatus.form) {
      dispatch(updateOrderStatus(EOrderStatus.approved))
    }
    redirectTo(`/panel/orders/${order?._id}`)
  }, [dispatch, order])


  const handleAppendOrder = useCallback(() => {
    redirectTo(`/panel/editor/${order?._id}`)
  }, [order])

  return (
    <>
      <aside
        className={cn(styles.aside, cartOpened ? styles.opened : undefined)}
      >
        <div className={styles.Cart}>
          <div>
            <Typography className={styles.title} as="span" styleAs="title-2">
              {`Детали заказа ${order ? order.number : ""}`}
            </Typography>
            <div className={styles.ItemsWrapper}>
              <div className={styles.Items}>
                {cart?.categories?.map((cat) => {
                  return <Category {...cat} key={cat.name} />;
                })}
              </div>
            </div>
            {cartTotal && cartTotal.cost > 0 ? (
              <Typography className={styles.total} as="span" styleAs="body">
                Итог: {cartTotal?.cost} руб.
              </Typography>
            ) : (
              <Typography color="#999" as="span" styleAs="body">
                Корзина пуста
              </Typography>
            )}
          </div>

          {cart && <div className={styles.actions}>
            {Close && (
              <SquareButton
                onClick={()=>{
                  setCartOpened(false)
                }}
                theme="default"
                size="lg"
                label="Свернуть"
                borders
                Icon={CloseIcon}
                className={styles.backBtn}
              />
            )}
            {Clear && (
              <SquareButton
                onClick={handleClearOrder}
                theme="danger"
                size="lg"
                label="Очистить"
                borders
                Icon={CloseIcon}
                active={cart?.categories && cart.categories.length > 0}
              />
            )}
            {Send && (
              <SquareButton
                onClick={handleSendOrder}
                theme="success"
                size="lg"
                label="Оформить"
                borders
                Icon={Check}
                active={cart?.categories && cart.categories.length > 0}
              />
            )}

            {Edit && (
              <SquareButton
                onClick={handleAppendOrder}
                theme='default'
                size="lg"
                label="Дополнить"
                borders
                Icon={Plus}
              />
            )}
          </div>}
        </div>
      </aside>
    </>
  );
};
