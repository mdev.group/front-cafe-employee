import { FunctionComponent } from "react";
import styles from "./Category.module.scss";
import cn from "classnames";
import { Item } from "./Item";
import { useDispatch } from "react-redux";
import { Typography } from "../Typography";
import { IOrderPosition } from "core/redux/types/dto/order";

type CategoryProps = {
  className?: string;
  name: string;
  items: Array<any>;
};

export const Category: FunctionComponent<CategoryProps> = ({
  className,
  name,
  items
}) => {
  const dispatch = useDispatch();

  return (
    <div className={styles.Category}>
      <Typography className={styles.title} as="span" styleAs="subhead">
        {name}
      </Typography>

      {items.map((item: IOrderPosition) => {
        return <Item item={item} key={item.uuid}/>;
      })}
    </div>
  );
};
