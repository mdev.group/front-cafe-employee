import { TFunc } from "core/types/types";
import { MouseEventHandler, useCallback, useLayoutEffect, useState } from "react";
import styles from "./ModalContainer.module.scss";

interface IProps {
  onClose: TFunc;
  hideCloseIcon?: boolean;
  title?: string;
}

const ModalContainer: React.FC<IProps> = ({
  children,
  onClose,
  hideCloseIcon,
  title
}) => {
  const [canClose, setCanClose] = useState(false);

  const handleClose = useCallback<MouseEventHandler>((e)=>{
    if(canClose && e.target === e.currentTarget) {
      onClose();
    }
  }, [onClose, canClose]);

  useLayoutEffect(() => {
    setTimeout(()=>{setCanClose(true)}, 500)
  }, [])

  return (
    <div className={styles.modal__wrapper} onClick={handleClose}>
      <div className={styles.modal}>
        {title && <span className={styles.modal__title}>{title}</span>}
        {!hideCloseIcon && <button onClick={handleClose} className={styles.modal__close} />}
        {children}
      </div>
    </div>
  )
}

export default ModalContainer;