import styles from "./ModalHeader.module.scss";

const ModalHeader: React.FC = ({
  children,
}) => { 
  return (
    <div className={styles.modalHeader}>
      {children}
    </div>
  )
}

export default ModalHeader;