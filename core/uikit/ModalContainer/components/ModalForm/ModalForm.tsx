import styles from "./ModalForm.module.scss";

const ModalForm: React.FC = ({
  children,
}) => { 
  return (
    <div className={styles.modalForm}>
      {children}
    </div>
  )
}

export default ModalForm;