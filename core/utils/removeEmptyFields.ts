export function removeEmptyFields (obj: any) {
  return Object.fromEntries(Object.keys(obj).filter((key)=> obj[key] !== undefined).map((key)=>{
    return [key, obj[key]];
  }))
}