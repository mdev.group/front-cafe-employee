import { useCallback, useMemo, useState } from "react"

export const useControlledValue = (controlledValue = undefined) => {
  const [value, setValue] = useState<any>(undefined);

  const finValue = useMemo(() => {
    return controlledValue !== undefined ? controlledValue : value;
  }, [controlledValue, value])

  return [finValue, setValue, controlledValue !== undefined]
}