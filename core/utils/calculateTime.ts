import { declOfNum } from "./devlOfNum"
import { numToLength } from "./numToLength"

export const calculateTime = (time: number) => {
  return `${numToLength(Math.floor(time / 60), 2)}:${numToLength(Math.floor(time % 60), 2)}`
}

export const calculateTimestamp = (time: number) => {
  const hours = Math.floor(time / 60);
  const mins = Math.floor(time % 60);

  const hoursText = declOfNum(hours, ['Час', 'Часа', 'Часов']);
  const minsText = declOfNum(mins, ['Минута', 'Минуты', 'Минут']);

  return `${hours} ${hoursText} ${mins > 0 ? `${mins} ${minsText}` : ''}`.trim()
}