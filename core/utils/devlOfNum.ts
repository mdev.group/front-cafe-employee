export function declOfNum(number: number, titles: [x1:string, x2:string, x7:string]) {  
  const cases: any = [2, 0, 1, 1, 1, 2];  
  return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}