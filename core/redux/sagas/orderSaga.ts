import delay from '@redux-saga/delay-p'
import redirectTo from 'core/utils/redirectTo';
import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import { v4 as uuidv4 } from 'uuid';
import api from '../../api';
import authTokenService from '../../services/authTokenService';

import {
  getUserData as getUserDataAction, setOwnedCafes, setUserData,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import type { IUserDTO } from '../types/dto/user';
import type { ICafeDTO } from '../types/dto/cafe';
import { AddToCart, checkLoyaltyCode, createOrder, createOrderSuccess, doOrderPayment, failCheckLoyaltyCode, getCafeOrders, IUpdateCartItem, removeFromCart, removeOrderSuccess, setLoyaltyUser, setOrders, successCheckLoyaltyCode, successGetcafeData, successSetLoyaltyUser, updateOrder, updateOrderStatus, updateOrderSuccess, updateToCart } from '../actions/cafe';
import { IComposition } from '../types/dto/item';
import { successGlobalCompositionRead } from '../actions/composition';
import { Action } from 'redux-actions';
import { EOrderStatus, IOrder, IOrderPosition } from '../types/dto/order';
import { cafe_selector, orders_selector, selectedOrder_selector } from '../selectors';
import { IClient } from '../types/dto/client';
import { playSound } from 'core/utils/playSound';

function* create({payload}: Action<any>): Generator {
  try {
    yield call(waitForAuth);
  
    const res = (yield call(api.createOrder, payload)) as IOrder;
    yield put(createOrderSuccess(res));

    redirectTo(`/panel/editor/${res._id}`)
  } catch (error) {
      yield put(requestError(error));
  }
}

function* removeItem({payload}: Action<string>): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      let newPositions: IOrderPosition[] = [];
      if(payload !== 'all') {
        newPositions = currOrderInfo.positions.filter((item) => !(payload !== 'all' && item.uuid == payload));
      }

      if (newPositions.length === 0) {
        yield call(api.deleteOrder, {id: currOrderInfo._id});
        yield put(removeOrderSuccess(currOrderInfo._id));
        redirectTo(`/panel/editor`)
      } else {
        const res = (yield call(api.updateOrder, {
          id: currOrderInfo._id,
          data: {
            positions: newPositions
          }
        })) as IOrder;
        yield put(updateOrderSuccess(res));
      }
    }
  } catch (error) {
      yield put(requestError(error));
  }
}

function* addItem({payload}: Action<IOrderPosition>): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      const newData = {...currOrderInfo};

      newData.positions = [...newData.positions, payload]

      const res = (yield call(api.updateOrder, {
        id: currOrderInfo._id,
        data: {
          positions: newData.positions
        }
      })) as IOrder;
      yield put(updateOrderSuccess(res));
    }
  } catch (error) {
      yield put(requestError(error));
  }
}

function* updItem({payload}: Action<IUpdateCartItem>): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      let positions;
      if(Array.isArray(payload.id)) {
        positions = currOrderInfo.positions.map((item) => {
          if(payload.id.includes(item.uuid)) {
            return {...item, ...payload.newData}
          }
          return item;
        })
      } else {
        positions = currOrderInfo.positions.map((item) => {
          if(item.uuid == payload.id) {
            return {...item, ...payload.newData}
          }
          return item;
        })
      }

      const res = (yield call(api.updateOrder, {
        id: currOrderInfo._id,
        data: {
          positions: positions || []
        }
      })) as IOrder;
      yield put(updateOrderSuccess(res));
    }
  } catch (error) {
      yield put(requestError(error));
  }
}

function* updStatus({payload}: Action<EOrderStatus>): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      const res = (yield call(api.updateOrder, {
        id: currOrderInfo._id,
        data: {
          status: payload
        }
      })) as IOrder;
      yield put(updateOrderSuccess(res));
    }
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getOrders(): Generator {
  try {
    yield call(waitForAuth);
  
    const currCafe = (yield select(cafe_selector)) as ICafeDTO;

    const res = (yield call(api.getActiveOrders, {
      cafeID: currCafe._id
    })) as IOrder[];

    yield put(setOrders(res));

  } catch (error) {
      yield put(requestError(error));
  }
}

function* update({payload}: Action<Partial<IOrder>>): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      const res = (yield call(api.updateOrder, {
        id: currOrderInfo._id,
        data: payload
      })) as IOrder;
      yield put(updateOrderSuccess(res));
    }
  } catch (error) {
      yield put(requestError(error));
  }
}

function* verifyLoyaltyCode({payload}: Action<string>): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      const res = (yield call(api.getClientByCode, {
        code: payload.toUpperCase()
      })) as IClient;

      if(!res) {
        throw new Error('No Client Found');
      }

      yield put(successCheckLoyaltyCode(res));
    }
  } catch (error) {
      yield put(requestError(error));
      yield put(failCheckLoyaltyCode('Wrong Code'));
  }
}

function* loyaltyUserWorker({payload}: Action<string | null>): Generator {
  try {
    yield call(waitForAuth);

    if(payload === null) {
      yield put(successSetLoyaltyUser(null));
    }
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      const res = (yield call(api.getClientByID, {
        id: payload!
      })) as IClient;

      if(!res) {
        throw new Error('No Client Found');
      }

      yield put(successSetLoyaltyUser(res));
    }
  } catch (error) {
      yield put(requestError(error));
      yield put(failCheckLoyaltyCode('Wrong Code'));
  }
}

function* doOrderPaymentWorker(): Generator {
  try {
    yield call(waitForAuth);
  
    const currOrderInfo = (yield select(selectedOrder_selector)) as IOrder;

    if(currOrderInfo) {
      const res = (yield call(api.doOrderPayment, {
        orderID: currOrderInfo._id
      })) as IOrder;
      yield put(updateOrderSuccess(res));
    }
  } catch (error) {
      yield put(requestError(error));
  }
}


function* getNewOrders(): Generator {
  try {
    yield call(waitForAuth);
  
    const currCafe = (yield select(cafe_selector)) as ICafeDTO;
    const orders = (yield select(orders_selector)) as IOrder[];

    const res = (yield call(api.getActiveOrders, {
      cafeID: currCafe._id
    })) as IOrder[];

    const newOrders = res.filter((it) => orders.every((ord) => ord._id !== it._id));
    if(newOrders.length > 0) {
      console.log(newOrders)
      playSound('/sounds/notify.wav');
    }

    yield put(setOrders(res));

  } catch (error) {
      yield put(requestError(error));
  }
}

function* fetchSagaPeriodically() {
  yield call(waitForAuth);

  while (true) {
    yield call(getNewOrders);
    yield delay(10000);
  }
}

export function* initOrderWatchers(){
  yield takeEvery(createOrder.toString(), create);
  yield takeEvery(removeFromCart.toString(), removeItem);
  yield takeEvery(AddToCart.toString(), addItem);
  yield takeEvery(updateToCart.toString(), updItem);
  yield takeEvery(updateOrderStatus.toString(), updStatus);
  yield takeEvery(updateOrder.toString(), update);
  yield takeEvery(getCafeOrders.toString(), getOrders);
  yield takeEvery(checkLoyaltyCode.toString(), verifyLoyaltyCode);
  yield takeEvery(setLoyaltyUser.toString(), loyaltyUserWorker);
  yield takeEvery(doOrderPayment.toString(), doOrderPaymentWorker);
  yield call(fetchSagaPeriodically)
}