import { call, select, take } from "redux-saga/effects";
import { IRootReducer } from "../reducers";

export function* waitFor(selector: any): Generator {
  if (yield select(selector)) return;

  while (true) {
    yield take('*');
    if (yield select(selector)) return;
  }
}

export function* waitForAuth(): Generator {
  const selectRefreshDataState = (state:IRootReducer) => state.auth.token;
  yield call(waitFor, selectRefreshDataState);
  
  console.log('[AUTH] AUTH FINISHED')

  return;
}
