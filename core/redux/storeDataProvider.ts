import { Store } from 'redux';

export type TStore = Store<any, any> & { dispatch: any };

interface TStoreDataProvider {
    get: () => TStore;
    set: (data: TStore) => void;
}

export const storeDataProvider = (): TStoreDataProvider => {
    let store: TStore;
    return {
        get: () => store,
        set: (data) => {
            store = data;
        }
    };
};
