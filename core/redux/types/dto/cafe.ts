import type { IEmployeeDTO } from "./employee";
import { IItemDTO } from "./item";
import type { IWorkShiftDTO } from "./workshift";

export enum ELoyaltyType {
  DISABLED,
  LOCAL,
  GLOBAL,
}

export interface ICafeDTO {
  _id: string;
  secret: string;
  ownerID: string;
  name: string;
  address: string;
  creationDate: number;
  closed: boolean;
  loyaltyType: ELoyaltyType;
  employees: IEmployeeDTO[];
  workshifts: IWorkShiftDTO[];
  items: IItemDTO[]
  photo_url?: string;
}

export interface IUpdateCafeDTO {
  name?: string;
  image?: string;
  loyaltyType?: ELoyaltyType;
  closed?: boolean;
}