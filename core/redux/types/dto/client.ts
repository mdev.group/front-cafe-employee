export interface IClient {
  _id: string;
  tgID: number;
  code: string;
  first_name: string;
  last_name: string;
  photo_url: string;
  reg_datetime: number;
  scores: number;
}