export enum EUserTypes {
  Global = 'GLOBAL',
  Employee = 'EMPLOYEE',
  Device = "DEVICE",
  Client = "CLIENT"
}

export enum EUserRoles {
  Admin = "ADMIN",
  User = "USER",
  Cafe = "CAFE",
  Guest = "GUEST",
  Owner = "OWNER"
}

export interface IUserDTO {
  first_name: string;
  last_name: string;
  photo_url: string;
  reg_datetime: number;
  tgID: number;
  _id: string;
}