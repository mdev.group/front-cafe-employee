
import { createAction } from "redux-actions";
import { ICafeDTO, IUpdateCafeDTO } from "../types/dto/cafe";
import { IClient } from "../types/dto/client";
import { IEmployeeDTO } from "../types/dto/employee";
import { EOrderStatus, IOrder, IOrderPosition } from "../types/dto/order";

export const successGetcafeData = createAction<ICafeDTO>(
  'SUCCESS_GET_CAFE_DATA'
);

export const getCafeOrders = createAction(
  'GET_CAFE_ORDERS'
);

export const setOrders = createAction<IOrder[]>(
  'SET_ORDERS'
);
export const setCurrentOrderID = createAction<string | null>(
  'SET_ORDER_ID'
);



export const createOrder = createAction<Partial<IOrder>>(
  'CREATE_ORDER'
);

export const createOrderSuccess = createAction<IOrder>(
  'SUCCESS_CREATE_ORDER'
);

export const updateOrder = createAction<Partial<IOrder>>(
  'UPDATE_ORDER'
);

export const updateOrderSuccess = createAction<IOrder>(
  'SUCCESS_UPDATE_ORDER'
);

export const removeOrderSuccess = createAction<string>(
  'SUCCESS_REMOVE_ORDER'
);

export interface IUpdateCartItem {
  id: string | string[];
  newData: Partial<IOrderPosition>;
}
export const updateToCart = createAction<IUpdateCartItem>(
  'UPDATE_TO_CART'
);

export const AddToCart = createAction<IOrderPosition>(
  'ADD_TO_CART'
);

export const removeFromCart = createAction<string>(
  'REMOVE_FROM_CART'
);

export const updateOrderStatus = createAction<EOrderStatus>(
  'UPDATE_ORDER_STATUS'
);

export const checkLoyaltyCode = createAction<string>(
  'CHECK_LOYALTY_CODE'
);
export const successCheckLoyaltyCode = createAction<IClient>(
  'SUCCESS_CHECK_LOYALTY_CODE'
);
export const failCheckLoyaltyCode = createAction<string>(
  'FAIL_CHECK_LOYALTY_CODE'
);

export const setLoyaltyUser = createAction<string | null>(
  'LOYALTY_SET_USER'
);
export const successSetLoyaltyUser = createAction<IClient | null>(
  'SUCCESS__LOYALTY_SET_USER'
);

export const doOrderPayment = createAction(
  'DO_ORDER_PAYMENT'
);