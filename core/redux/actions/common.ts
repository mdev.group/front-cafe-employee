import { createAction } from "redux-actions";

export interface IRequestErrorPayload {
  errorMessage: any;
}
export const requestError = createAction<IRequestErrorPayload, any>(
  "REQUEST_ERROR",
  (error: any) => ({ errorMessage: error })
);