import { createAction } from "redux-actions";
import { IEmployeeDTO } from "../types/dto/employee";
import { IComposition, IItemDTO, IPFC } from "../types/dto/item";


export type beginItemCreate_payload = {
  name: number;
  cafeID: string;
};
export const beginItemCreate = createAction<beginItemCreate_payload>(
  'BEGIN_ITEM_CREATE'
);
export const successCompositionCreate = createAction<IComposition>(
  'SUCCESS_ITEM_CREATE'
);
export const failCompositionCreate = createAction<string>(
  'FAIL_ITEM_CREATE'
);



export type beginItemUpdate_payload = {
  id: string;
  newData: Partial<IItemDTO>;
};
export const beginItemUpdate = createAction<beginItemUpdate_payload>(
  'BEGIN_ITEM_UPDATE'
);
export const successItemUpdate = createAction<IComposition>(
  'SUCCESS_ITEM_UPDATE'
);
