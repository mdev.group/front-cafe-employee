import { createAction } from "redux-actions";
import { IEmployeeDTO } from "../types/dto/employee";
import { IComposition, IPFC } from "../types/dto/item";


export type beginCompositionCreate_payload = {
  name: number;
  pfc?: IPFC;
};
export const beginCompositionCreate = createAction<beginCompositionCreate_payload>(
  'BEGIN_COMPOSITION_CREATE'
);
export const successCompositionCreate = createAction<IComposition>(
  'SUCCESS_COMPOSITION_CREATE'
);
export const failCompositionCreate = createAction<string>(
  'FAIL_COMPOSITION_CREATE'
);


export const beginGlobalCompositionRead = createAction(
  'BEGIN_GLOBAL_COMPOSITION_READ'
);
export const successGlobalCompositionRead = createAction<IComposition[]>(
  'SUCCESS_GLOBAL_COMPOSITION_READ'
);
export const failGlobalCompositionRead = createAction<string>(
  'FAIL_GLOBAL_COMPOSITION_READ'
);