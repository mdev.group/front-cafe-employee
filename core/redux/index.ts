import { createStore, applyMiddleware, combineReducers, ReducersMapObject, compose } from "redux";
import createSagaMiddleware, { Saga } from 'redux-saga';

import { storeDataProvider, TStore } from './storeDataProvider'
import { createRootSaga } from "./utils";

import rootReducer from './reducers'
import rootSagas from './sagas'

const storeData = storeDataProvider();
const sagaMiddleware = createSagaMiddleware();

const middleware = applyMiddleware(
  sagaMiddleware
);

type TInitStore = () => void;
export const initStore: TInitStore = () => {
  
  const composeEnhancers = typeof window !== 'undefined' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose;

  const combinedReducer = combineReducers(rootReducer);
  const rootSaga: Saga<[]> = createRootSaga([rootSagas]);
  const composedData = composeEnhancers(middleware);
  const store = createStore(combinedReducer, {}, composedData);

  sagaMiddleware.run(rootSaga);

  storeData.set(store);
};

let _storeData: TStore;

export const getStore = ()=>{
  if(_storeData){
    return _storeData;
  } else {
    _storeData = storeData.get()
    return _storeData;
  }
};