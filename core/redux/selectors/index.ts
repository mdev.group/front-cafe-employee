import { createSelector } from "@reduxjs/toolkit"
import { IRootReducer } from "../reducers"
import { IItemDTO } from "../types/dto/item";
import { IOrder, IOrderPosition } from "../types/dto/order";

export const user_selector = (state: IRootReducer) => state.user.userData;
export const cafe_selector = (state: IRootReducer) => state.user.cafe;
export const orders_selector = (state: IRootReducer) => state.user.orders;
export const selectedOrderID_selector = (state: IRootReducer) => state.user.currentOrderID;
export const compositions_selector = (state: IRootReducer) => state.compostions.global;
export const checkLoyalty_selector = (state: IRootReducer) => state.user.checkLoyalty;

export const cafeItems_selector = createSelector([cafe_selector], (cafe) => cafe?.items);
export const cafeCategories_selector = createSelector([cafeItems_selector], (items) => items?.reduce((prev, item) => {
  if(!prev.includes(item.category || 'Без категории')) {
    prev.push(item.category || 'Без категории');
    return prev;
  }
  return prev;
}, [] as string[]));
export const selectedOrder_selector = createSelector([orders_selector, selectedOrderID_selector], (orders, currID) => orders?.find((item) => item._id === currID) || null)

export const cafeItemByID_selector = (ID: string) => createSelector([cafeItems_selector], (items) => items?.find((item) => item._id === ID) || null)
export const cafeIngredientByID_selector = (ID: string) => createSelector([compositions_selector], (items) => items?.find((item) => item._id === ID) || null)

interface ICategory {
  name: string;
  items: IOrderPosition[]
}

export const selectedOrderCart_selector = createSelector([selectedOrder_selector, cafeItems_selector, compositions_selector], (order, items, comps) => {
  if(order) {
    const categories = order.positions.reduce((prev, curr) => {
      const item = items?.find((item) => curr.itemID === item._id)

      if(item) {
        let catIndex = 0
        if(item.category) {
          catIndex = prev.findIndex((cat) => cat.name == item.category)
        }

        if(catIndex == -1) {
          prev.push({
            name: item.category!,
            items: [curr]
          })
        } else {
          prev[catIndex].items.push(curr)
        }
      }

      return prev;
    }, [{
      name: 'Без категории',
      items: []
    }] as ICategory[])
    .filter((item) => item.items.length > 0);

    return {
      ...order,
      categories
    }
  }
})


export const selectedOrderCartTotal_selector = createSelector([selectedOrderCart_selector, cafeItems_selector], (cart, items) => {
  const cost = cart?.positions.reduce((prev, position) => {
    const item = items?.find((item) => position.itemID === item._id)

    let baseCost = 0;
    let additiveCost = 0;
    if(item) {
      baseCost = item.price || 0
      additiveCost = position.ingredients.reduce((prev, ing) => {
        const count = Math.max(0, ing.count - ing.defaultCount);
        const price = count*ing.price;
        return prev + price;
      }, 0);
    }
    const finCost = baseCost + additiveCost;

    return prev + finCost;
  }, 0) || 0


  return {
    cost
  }
})