import { handleActions, Action } from 'redux-actions';
import { successGlobalCompositionRead } from '../actions/composition';

import { IComposition } from '../types/dto/item';

export interface ICompostionState {
  global?: IComposition[];
}

const initialState: ICompostionState = {
  global: undefined
};

// TODO: remove all any
const reducer = handleActions<ICompostionState, any>({
  [successGlobalCompositionRead.toString()]: (state, {payload}: Action<IComposition[]>) => ({
    ...state,
    global: payload
  }),
}, initialState);

export default reducer;
