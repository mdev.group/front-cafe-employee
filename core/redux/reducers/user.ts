import { createSelector } from '@reduxjs/toolkit';
import mergeDeep from 'core/utils/mergeDeep';
import { uniqueId } from 'lodash';
import { handleActions, Action } from 'redux-actions';
import { AddToCart, checkLoyaltyCode, createOrder, createOrderSuccess, failCheckLoyaltyCode, IUpdateCartItem, removeFromCart, removeOrderSuccess, setCurrentOrderID, setOrders, successCheckLoyaltyCode, successGetcafeData, successSetLoyaltyUser, updateOrderSuccess, updateToCart } from '../actions/cafe';

import { setOwnedCafes, setUserData } from '../actions/user';

import type { ICafeDTO } from '../types/dto/cafe';
import { IClient } from '../types/dto/client';
import type { IOrder, IOrderPosition } from '../types/dto/order';
import type { IUserDTO } from '../types/dto/user';

export interface IUsertate {
  userData?: IUserDTO;
  cafe?: ICafeDTO;
  orders: IOrder[];
  currentOrderID?: string | null;

  checkLoyalty?: {
    res?: IClient | null,
    code?: string;
  }
}

const initialState: IUsertate = {
  userData: undefined,
  cafe: undefined,
  orders: [],
  currentOrderID: undefined,
};

// TODO: remove all any
const reducer = handleActions<IUsertate, any>({
  [setUserData.toString()]: (state, {payload}: Action<IUserDTO>) => ({
    ...state,
    userData: payload
  }),
  
  [successGetcafeData.toString()]: (state, {payload}: Action<ICafeDTO>) => ({
    ...state,
    cafe: payload
  }),

  [setOrders.toString()]: (state, {payload}: Action<IOrder[]>) => ({
    ...state,
    orders: payload
  }),

  [setCurrentOrderID.toString()]: (state, {payload}: Action<string>) => ({
    ...state,
    currentOrderID: payload
  }),

  [createOrderSuccess.toString()]: (state, {payload}: Action<IOrder>) => ({
    ...state,
    orders: [...state.orders, payload],
    currentOrderID: payload._id
  }),

  [updateOrderSuccess.toString()]: (state, {payload}: Action<IOrder>) => ({
    ...state,
    orders: state.orders.map((ord) => {
      if(ord._id === payload._id) {
        return mergeDeep(ord, payload);
      }
      return ord;
    })
  }),

  [removeOrderSuccess.toString()]: (state, {payload}: Action<string>) => ({
    ...state,
    orders: state.orders.filter((ord) => ord._id !== payload),
    currentOrderID: null
  }),

  [checkLoyaltyCode.toString()]: (state, {payload}: Action<string>) => ({
    ...state,
    checkLoyalty: {
      res: undefined,
      code: payload
    }
  }),

  [successCheckLoyaltyCode.toString()]: (state, {payload}: Action<IClient>) => ({
    ...state,
    checkLoyalty: {
      ...state.checkLoyalty,
      res: payload,
    }
  }),

  [failCheckLoyaltyCode.toString()]: (state, {payload}: Action<string>) => ({
    ...state,
    checkLoyalty: {
      res: null,
      code: payload
    }
  }),

  [successSetLoyaltyUser.toString()]: (state, {payload}: Action<IClient | null>) => ({
    ...state,
    checkLoyalty: {
      res: payload
    }
  })


}, initialState);

export default reducer;
