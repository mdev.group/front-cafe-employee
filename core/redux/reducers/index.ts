import { ReducersMapObject } from 'redux';

import auth, { IAuthState } from './auth';
import compostions, { ICompostionState } from './composition';
import user, { IUsertate } from './user';

export interface IRootReducer{
    auth: IAuthState,
    user: IUsertate,
    compostions: ICompostionState;
}

const reducers: ReducersMapObject<IRootReducer, any> = {
    auth,
    user,
    compostions
};

export default reducers;
