const LOCAL_STORAGE_FIELDS = {
    TOKEN: '_temp_UI_Id',
    ACCESS_TOKEN_LIFETIME: 'accessTokenLifeTime',
    REFRESH_TOKEN: 'refreshToken',
};

export default LOCAL_STORAGE_FIELDS;
